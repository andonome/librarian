# Contribution Guide {#contributing}

## Reporting bugs

All development issues are tracked on [GitLab][gitlab-issues]. Please make sure
that your issue has not been reported already.

You can create a new bug [here][gitlab-new-issue-bug].

## Localization

Librarian implements localization using a system called [gettext].

To help translate it into your native language you need to distinguish a couple
of key files:

- `po/librarian.pot` --- contains all strings that are meant to be translated 
  and serves as a template for concrete translation files.
  It is (re)generated by running `make extract-strings`.
- `po/LANG/librarian.po` --- a specialization of `po/librarian.pot` which
  contains translated strings for a particular language `LANG`.

Typical workflow looks like this:

1. Extract new strings by running `make extract-strings`.
2. If you are the first one translating for that language:
    1. Add the appropriate language code in the `po/LANGUAGES` file.
       [Here][gettext-lang-codes] is a list of available language codes.
    2. Run `make create-translations-<LANG>`. This will create
       `po/LANG/librarian.po` file which should contain translated strings for
       your particular language LANG.
    3. Fill in the metadata header in `po/LANG/librarian.po`.
3. Translate the strings in `po/LANG/librarian.po`. To make sure that it is in
   sync with `po/librarian.pot` run `make update-translations`.
4. Rebuild and install the project.

Once you are satisfied with your changes submit a merge request.

## Development

The basic steps for building Librarian are described in the [README].

Linux container image used on GitLab CI is defined in the `Dockerfile`.

### Coding style

Librarian's coding style is enforced using [clang-tidy] and [clang-format].
Behavior-related rules are specified in `.clang-tidy` while formatting rules are
specified in `.clang-format`.

Indentation and line lengths are enforced using [EditorConfig][editor-config]
and specified in the `.editorconfig` file.

Informally, all lines should be at most 80 columns long and end with a
UNIX-style ending `\n`. All indents should be made with 4 spaces.
Exceptions are languages where tabs are necessary (e.g. Make).

You are encouraged to use an editor that integrates well with these tools.

@note
These specifications are not exhaustive.
In case that it is not clear how to format your code try to imitate the style of
related documents.

[README]: https://gitlab.com/dusan-gvozdenovic/librarian/-/blob/master/README.md
[gitlab-issues]: https://gitlab.com/dusan-gvozdenovic/librarian/-/issues
[gitlab-new-issue-bug]: https://gitlab.com/dusan-gvozdenovic/librarian/-/issues/new?issuable_template=Bug
[gettext]: https://www.gnu.org/software/gettext
[gettext-lang-codes]: https://www.gnu.org/software/gettext/manual/html_node/Usual-Language-Codes.html
[clang-tidy]: https://clang.llvm.org/extra/clang-tidy
[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[editor-config]: https://editorconfig.org
