/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_TYPES_H__
#define __LIBRARIAN_TYPES_H__

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>

#include "utf8.h"

typedef int32_t exit_status;
typedef uint64_t hash;

#endif
