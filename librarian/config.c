/* SPDX-License-Identifier: Apache-2.0 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <ncurses.h>

#include "command.h"
#include "config.h"
#include "core.h"
#include "file.h"
#include "library.h"
#include "limits.h"
#include "localization.h"
#include "log.h"
#include "observer.h"
#include "types.h"
#include "u-string.h"
#include "xmalloc.h"

// Maybe find something more adequate?
#define CONFIG_BUFFER_SIZE (NAME_MAX + 255)
#define COMMAND_LINE_TOKEN_SIZE 31

static int color_highlight_bg = COLOR_BLUE;
static int color_highlight_fg = COLOR_WHITE;
static int color_selection_bg = COLOR_WHITE;
static int color_selection_fg = COLOR_BLACK;
static int color_info_bg = COLOR_WHITE;
static int color_info_fg = COLOR_BLACK;
static int color_warning_bg = COLOR_YELLOW;
static int color_warning_fg = COLOR_BLACK;
static int color_error_bg = COLOR_RED;
static int color_error_fg = COLOR_WHITE;
static int color_text_primary = COLOR_GREEN;
static bool show_line_numbers = true;
static char colorscheme[NAME_MAX] = "";
static char command_line_token[COMMAND_LINE_TOKEN_SIZE] = ":";
#ifdef __linux__
static char open_command_format[CONFIG_OPEN_COMMAND_FORMAT_SIZE] =
    "xdg-open \"%s\" 2>&1";
#elif defined(__APPLE__) && defined(__MACH__)
#define FMT_STR "open \"%s\" 2>&1"
static char open_command_format[CONFIG_OPEN_COMMAND_FORMAT_SIZE] =
    "open \"%s\" 2>&1";
#endif

static char runtime_path[30 * (PATH_MAX + 2)] =
    LIBRARIAN_SYSTEM_PREFIX "/share/librarian/plugins";

// static const char *default_view = DEFAULT_VIEW;

static const char *ORDER_VALUES[] = { "author", "title", "date", NULL };

static const char *order = CONFIG_ORDER_DEFAULT;

#define OPT(NAME, ABBR, TYPE, VAR)                                             \
    { { NAME, ABBR, TYPE }, VAR }
#define BOOL_OPT(NAME, ABBR, VAR) OPT(NAME, ABBR, BOOL_OPTION, VAR)
#define INT_OPT(NAME, ABBR, VAR) OPT(NAME, ABBR, INT_OPTION, VAR)
#define COLOR_OPT(NAME, ABBR, VAR) OPT(NAME, ABBR, COLOR_OPTION, VAR)
#define STRING_OPT(NAME, ABBR, VALIDATOR, VAR, CAPACITY)                       \
    { { NAME, ABBR, STRING_OPTION }, VAR, CAPACITY, VALIDATOR }
#define ENUM_OPT(NAME, ABBR, ENUMS, VAR)                                       \
    { { NAME, ABBR, ENUM_OPTION }, ENUMS, VAR }

// clang-format off
struct config config = {
    .path = PATH_INIT,
    .show_line_numbers = BOOL_OPT("number", "nu", &show_line_numbers),
    .color_highlight_bg = COLOR_OPT("color_highlight_bg", NULL, &color_highlight_bg),
    .color_highlight_fg = COLOR_OPT("color_highlight_fg", NULL, &color_highlight_fg),
    .color_selection_bg = COLOR_OPT("color_selection_bg", NULL, &color_selection_bg),
    .color_selection_fg = COLOR_OPT("color_selection_fg", NULL, &color_selection_fg),
    .color_info_bg = COLOR_OPT("color_info_bg", NULL, &color_info_bg),
    .color_info_fg = COLOR_OPT("color_info_fg", NULL, &color_info_fg),
    .color_warning_bg = COLOR_OPT("color_warning_bg", NULL, &color_warning_bg),
    .color_warning_fg = COLOR_OPT("color_warning_fg", NULL, &color_warning_fg),
    .color_error_bg = COLOR_OPT("color_error_bg", NULL, &color_error_bg),
    .color_error_fg = COLOR_OPT("color_error_fg", NULL, &color_error_fg),
    .color_text_primary = COLOR_OPT("color_text_primary", NULL, &color_text_primary),
    .colorscheme = STRING_OPT("colorscheme", NULL, NULL, colorscheme, NAME_MAX),
    .command_line_token = STRING_OPT(
        "command_line_token", "cmdtok", NULL,
        command_line_token, COMMAND_LINE_TOKEN_SIZE),
    // .default_view = ENUM_OPT("default_view", NULL, views, &default_view)
    .open_command_format = STRING_OPT(
            "open_command_format", "opfmt", NULL,
            open_command_format, CONFIG_OPEN_COMMAND_FORMAT_SIZE),
    .runtime_path  = STRING_OPT(
            "runtime_path", "rtp", NULL,
            runtime_path, CONFIG_OPEN_COMMAND_FORMAT_SIZE),
    .order = ENUM_OPT("order", NULL, ORDER_VALUES, &order)
};

static const struct option *options[] = {
    &config.show_line_numbers.option,
    &config.color_highlight_bg.option, &config.color_highlight_fg.option,
    &config.color_selection_bg.option, &config.color_selection_fg.option,
    &config.color_info_bg.option, &config.color_info_fg.option,
    &config.color_warning_bg.option, &config.color_warning_fg.option,
    &config.color_error_bg.option, &config.color_error_fg.option,
    &config.color_text_primary.option,
    &config.colorscheme.option,
    &config.command_line_token.option,
    // &config.default_view.option,
    &config.open_command_format.option,
    &config.runtime_path.option,
    &config.order.option,
    NULL
};
// clang-format on

static struct path _config_base_path = PATH_INIT;
static struct command_parser config_cmd_parser;

/* -------------------- Internal function declarations ---------------------- */

static void _config_ensure(void);

static void _config_init_cmd_parser(void);

void _config_notify_observer(struct observer *observer,
    struct config_args *config_args);

/* ---------------------------- Implementation ------------------------------ */

void config_init(void) {
    _config_ensure();
    _config_init_cmd_parser();
    config_load();
    config.on_change = SUBJECT_INIT(_config_notify_observer);
    config.on_color_change = SUBJECT_INIT(_config_notify_observer);
}

static void _config_ensure(void) {
    // Load base path
    path_set_c_str(&_config_base_path, LIBRARIAN_CONFIG_PATH,
        c_str_static_length(LIBRARIAN_CONFIG_PATH));

    // XXX: Assuming LIBRARIAN_CONFIG_PATH specified at compile time is correct.
    (void) path_expand(&_config_base_path);

    config_get_path(&config.path, "config");

    bool success;

    success = path_create_directory(&_config_base_path);
    if (!success) {
        log_error("%s", _("Failed to create config directory. Exiting now."));
        librarian_exit(EXIT_FAILURE);
    }

    // BUG: Might be something else
    if (!path_is_file(&config.path)) {
        success = path_create_file(&config.path, "");

        if (!success) {
            log_error("%s", _("Failed to create config file. Exiting now."));
            librarian_exit(EXIT_FAILURE);
        }
    }
}

static void _config_init_cmd_parser(void) {
    command_parser_init(&config_cmd_parser);
    // TODO: Make this use only `set` command. Will need some refactoring
    commands_core_register(&config_cmd_parser);
}

void _config_notify_observer(struct observer *observer,
    struct config_args *config_args) {
    ((void (*)(struct config_args *))(observer->notify))(config_args);
}

void config_load(void) {
    FILE *fp = fopen(config.path.str.c_str, "r");

    if (!fp) {
        const char *msg = _("Failed to open config file \"%s\" for reading.");
        log_error(msg, config.path);
        return;
    }

    char buffer[CONFIG_BUFFER_SIZE];

    while (fgets(buffer, CONFIG_BUFFER_SIZE, fp)) {
        buffer[strcspn(buffer, "\n")] = '\0';
        config_parse(buffer);
    }

    fclose(fp);
}

void config_parse(const char *buffer) {
    bool success = command_parser_execute(&config_cmd_parser, buffer);
    if (!success) { log_error(_("Failed to parse \"%s\"."), buffer); }
}

const struct option *config_find_option(const char *name) {
    const struct option **opt = options;
    for (; *opt && !option_named(*opt, name); opt++) { }
    if (!*opt) { return NULL; }
    return *opt;
}

enum config_result config_set(const char *key, const char *value) {
    const struct option *opt = config_find_option(key);

    if (!opt) { return STATUS_NO_KEY; }

    enum config_result result;

    switch (opt->type) {
        case BOOL_OPTION:
            result = config_set_bool(opt, value);
            break;
        case STRING_OPTION:
            result = config_set_string(opt, value);
            break;
        case COLOR_OPTION:
            result = config_set_color(opt, value);
            break;
        case INT_OPTION:
            result = config_set_int(opt, value);
            break;
        default: // ENUM_OPTION
            result = config_set_enum(opt, value);
            break;
    }

    return result;
}

enum config_result
config_set_color(const struct option *opt, const char *value) {
    int color = str_to_term_color(value);
    if (color == INVALID_TERM_COLOR) { return STATUS_INVALID_COLOR; }
    int_option_set(opt, color);
    struct config_args *args = &(struct config_args){ opt };
    subject_notify_observers(&config.on_color_change, args);
    return STATUS_OK;
}

enum config_result config_set_int(const struct option *opt, const char *value) {
    char *end;
    int val = strtol(value, &end, 10);
    if (!end || end == value || errno == ERANGE) { return STATUS_INVALID_INT; }
    int_option_set(opt, val);
    subject_notify_observers(&config.on_change, &(struct config_args){ opt });
    return STATUS_OK;
}

enum config_result config_set_bool_val(const struct option *opt, bool value) {
    bool_option_set(opt, value);
    subject_notify_observers(&config.on_change, &(struct config_args){ opt });
    return STATUS_OK;
}

enum config_result
config_set_bool(const struct option *opt, const char *value) {
    if (strcmp(value, "true") && strcmp(value, "false")) {
        return STATUS_INVALID_BOOL;
    }
    return config_set_bool_val(opt, *value == 't');
}

enum config_result
config_set_string(const struct option *opt, const char *value) {
    string_option_set(opt, value);
    subject_notify_observers(&config.on_change, &(struct config_args){ opt });
    return STATUS_OK;
}

enum config_result
config_set_enum(const struct option *opt, const char *value) {
    enum config_result result = STATUS_INVALID_ENUM;
    bool success = enum_option_set(opt, value);
    if (success) {
        subject_notify_observers(&config.on_change,
            &(struct config_args){ opt });
        result = STATUS_OK;
    }
    return result;
}

void config_get_path(struct path *path, const char *relative) {
    path_copy(path, &_config_base_path);
    path_append(path, relative);
}

void config_cleanup(void) {
    subject_unsubscribe_all(&config.on_change);
    subject_unsubscribe_all(&config.on_color_change);
    command_parser_cleanup(&config_cmd_parser);
    path_cleanup(&_config_base_path);
    path_cleanup(&config.path);
}
