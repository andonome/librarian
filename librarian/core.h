/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_CORE_H__
#define __LIBRARIAN_CORE_H__

#include "compiler.h"
#include "observer.h"

#ifndef LIBRARIAN_SYSTEM_PREFIX
#error "Expected LIBRARIAN_SYSTEM_PREFIX to be defined!"
#endif

struct lifecycle {
    struct subject on_init;
    struct subject on_exit;
};

extern struct lifecycle lifecycle;

struct lifecycle_exit_args {
    int status;
};

void librarian_init(void);

attr_noreturn void librarian_exit(int status);

#endif
