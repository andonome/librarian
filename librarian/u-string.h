/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_U_STRING_H__
#define __LIBRARIAN_U_STRING_H__

#define INITIAL_CAPACITY 256

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "compiler.h"
#include "types.h"
#include "utf8.h"
#include "utils.h"
#include "xmalloc.h"

struct u_string {
    char *c_str;
    size_t length;
    size_t byte_length;
    size_t capacity;
};

/**
 * Initial value of u_string.
 */
#define U_STRING_INIT_STATIC                                                   \
    { .capacity = 0, .byte_length = 0, .length = 0, .c_str = NULL }

#define U_STRING_INIT ((struct u_string) U_STRING_INIT_STATIC)

/**
 * Compares strings str1 and str2.
 *
 * @remark This function is null-safe; If one of the arguments is null, they
 * will be treated as empty strings.
 * @related u_string
 */
int u_string_compare(const struct u_string *str1, const struct u_string *str2);

/** @see u_string_set_c_str_n */
bool u_string_set_c_str_n(struct u_string *str, const char *c_str,
    size_t n_bytes);

/**
 * Sets @ref c_str to provided string structure (@ref str), while precalculating
 * lengths for future use.
 *
 * @param str   Unicode string structure.
 * @param c_str Char pointer (c-style string).
 *
 * @remark If the length of c_str exceeds current capacity, the string will be
 * resized to hold exactly the content of c_str.
 * @memberof u_string
 */
bool u_string_set_c_str(struct u_string *str, const char *c_str);

void u_string_set(struct u_string *destination, const struct u_string *source);

void u_string_append_c_str(struct u_string *str, const char *c_str,
    size_t length);

ssize_t u_string_vprintf(struct u_string *str, const char *format, va_list arg);

// clang-format off
attr_format(__printf__, 2, 3)
ssize_t u_string_printf(struct u_string *str, const char *format, ...);
// clang-format on

/**
 * Frees the underlying string buffer and restores the container to its initial
 * state.
 * @memberof u_string
 */
void u_string_cleanup(struct u_string *str);

#endif
