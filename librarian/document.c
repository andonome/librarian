/* SPDX-License-Identifier: Apache-2.0 */

#include "document.h"
#include "file.h"
#include "rbtree.h"
#include "serializer.h"
#include "u-string.h"
#include "xmalloc.h"

struct document *document_new(void) {
    struct document *doc = xnew(struct document);
    doc->path = PATH_INIT;
    doc->author = U_STRING_INIT;
    doc->title = U_STRING_INIT;
    doc->ref = REF_INIT;
    rbnode_init(&doc->tree_node);
    return doc;
}

void document_destroy(struct document **doc) {
    if (!doc || !*doc) { return; }
    path_cleanup(&(*doc)->path);
    u_string_cleanup(&(*doc)->author);
    u_string_cleanup(&(*doc)->title);
    xfree_ptr(doc);
}

int document_compare_by_author(const struct document *doc1,
    const struct document *doc2) {
    int cmp;
    cmp = u_string_compare(&doc1->author, &doc2->author);
    if (cmp) { return cmp; }

    cmp = u_string_compare(&doc1->title, &doc2->title);
    if (cmp) { return cmp; }

    cmp = path_compare(&doc1->path, &doc2->path);
    return cmp;
}

int document_compare_by_title(const struct document *doc1,
    const struct document *doc2) {
    int cmp;
    cmp = u_string_compare(&doc1->title, &doc2->title);
    if (cmp) { return cmp; }

    cmp = u_string_compare(&doc1->author, &doc2->author);
    if (cmp) { return cmp; }

    cmp = path_compare(&doc1->path, &doc2->path);
    return cmp;
}

int document_compare_by_date(const struct document *doc1,
    const struct document *doc2) {
    int cmp;
    cmp = date_compare(&doc1->create_date, &doc2->create_date);
    if (cmp) { return cmp; }

    cmp = path_compare(&doc1->path, &doc2->path);
    return cmp;
}

bool document_serialize(struct document *doc, struct serializer *sz) {
    return serializer_write_string(sz, doc->path.str.c_str ?: "",
               doc->path.str.byte_length) &&
        serializer_write_string(sz, doc->title.c_str ?: "",
            doc->title.byte_length) &&
        serializer_write_string(sz, doc->author.c_str ?: "",
            doc->author.byte_length) &&
        serializer_write_int(sz, sizeof(doc->create_date.day),
            doc->create_date.day) &&
        serializer_write_int(sz, sizeof(doc->create_date.month),
            doc->create_date.month) &&
        serializer_write_int(sz, sizeof(doc->create_date.year),
            doc->create_date.year);
}

bool document_deserialize(struct document *doc, struct serializer *sz) {
    bool success = false;
    size_t length;
    char *buffer = xnew_n(char, PATH_MAX);

    // TODO: test if serializer_read_string calls need reallocation
    // and resize buffer

    length = serializer_read_string(sz, buffer, PATH_MAX);
    if (length == -1) { goto exit; }
    if (length > 0) { u_string_set_c_str(&doc->path.str, buffer); }

    length = serializer_read_string(sz, buffer, PATH_MAX);
    if (length == -1) { goto exit; }
    if (length > 0) { u_string_set_c_str(&doc->title, buffer); }

    length = serializer_read_string(sz, buffer, PATH_MAX);
    if (length == -1) { goto exit; }
    if (length > 0) { u_string_set_c_str(&doc->author, buffer); }

    success = serializer_read_int(sz, sizeof(doc->create_date.day),
        &doc->create_date.day);
    if (!success) { goto exit; }

    success = serializer_read_int(sz, sizeof(doc->create_date.month),
        &doc->create_date.month);
    if (!success) { goto exit; }

    success = serializer_read_int(sz, sizeof(doc->create_date.year),
        &doc->create_date.year);

exit:
    xfree(buffer);
    return success;
}
