/*
 * rbtree.h -- generic red-black tree
 *
 * Copyright (c) 2001-2007, NLnet Labs. All rights reserved.
 *
 * This software is open source.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the NLNET LABS nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file
 * Red black tree. Implementation taken from NSD 3.0.5, adjusted for use
 * in unbound (memory allocation, logging and so on).
 */

#ifndef __RBTREE_H__
#define __RBTREE_H__

#include "compiler.h"

typedef char rbnode_color;

#define RBNODE_BLACK 0
#define RBNODE_RED 1

/**
 * The struct rbnode struct definition.
 */
struct rbnode {
    /** parent in rbtree, NULL for root */
    struct rbnode *parent;
    /** left node (smaller items) */
    struct rbnode *left;
    /** right node (larger items) */
    struct rbnode *right;
    /** colour of this node */
    rbnode_color color;
};

/** definition for tree struct */
struct rbtree {
    struct rbnode *root;
};

#define RBTREE                                                                 \
    (struct rbtree) { .root = NULL }

#define RBNODE_INIT                                                            \
    { NULL, NULL, NULL, RBNODE_RED }

#define rbtree_entry(ptr, type, member) container_of(ptr, type, member)

static inline void rbnode_init(struct rbnode *node) {
    node->left = node->right = node->parent = NULL;
    node->color = RBNODE_RED;
}

static inline void
rbnode_link(struct rbnode *node, struct rbnode *parent, struct rbnode **link) {
    node->parent = parent;
    node->left = node->right = NULL;
    *link = node;
}

// TODO: Write docs. Called to rebalance red-black tree.
void rbtree_insert_fixup(struct rbtree *rbtree, struct rbnode *node);

// TODO: Write docs. Delete element from tree.
void rbtree_delete(struct rbtree *rbtree, struct rbnode *to_delete);

/**
 * Returns first (smallest) node in the tree
 * @param rbtree: tree
 * @return: smallest element or NULL if tree empty.
 */
struct rbnode *rbtree_first(struct rbtree *rbtree);

/**
 * Returns last (largest) node in the tree
 * @param rbtree: tree
 * @return: largest element or NULL if tree empty.
 */
struct rbnode *rbtree_last(struct rbtree *rbtree);

/**
 * Returns next larger node in the tree
 * @param rbtree: tree
 * @return: next larger element or NULL if no larger in tree.
 */
struct rbnode *rbtree_next(struct rbnode *rbtree);

/**
 * Returns previous smaller node in the tree
 * @param rbtree: tree
 * @return: previous smaller element or NULL if no previous in tree.
 */
struct rbnode *rbtree_previous(struct rbnode *rbtree);

#define rbtree_for(iter, tree)                                                 \
    for (iter = rbtree_first(tree); iter; iter = rbtree_next(iter))

#define rbtree_for_each_entry(iter, tree, member)                              \
    for (iter = container_of_safe(rbtree_first((tree)), __typeof__(*iter),     \
             member);                                                          \
         iter != NULL; iter = container_of_safe(rbtree_next(&(iter)->member),  \
                           __typeof__(*iter), member))

#endif /* UTIL_RBTREE_H_ */
