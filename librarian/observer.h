/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_OBSERVER_H__
#define __LIBRARIAN_OBSERVER_H__

#include "dequeue.h"
#include "xmalloc.h"

struct observer {
    void *notify;
    struct dequeue_head head;
};

typedef void (*notifier)(struct observer *observer, void *args);

struct subject {
    struct dequeue_head observers;
    notifier notifier;
};

/** @brief Creates and initializes a new subject. */
#define SUBJECT_INIT(NOTIFIER)                                                 \
    ((struct subject){ DEQUEUE_EMPTY, (notifier) NOTIFIER })

/**
 * Iterates through subscribed observers and notifies them about changes
 * that have been made.
 *
 * @memberof subject
 */
static inline void
subject_notify_observers(struct subject *subject, void *args) {
    if (!subject->notifier || !dequeue_previous(&subject->observers) ||
        !dequeue_next(&subject->observers)) {
        return;
    }

    struct observer *current;
    dequeue_for_each_entry (current, &subject->observers, head) {
        subject->notifier(current, args);
    }
}

/**
 * Initializes the observer.
 * @param[in] observer The observer to be initialized.
 */
static inline void observer_init(struct observer *observer, void *notify) {
    observer->notify = notify;
    dequeue_init(&observer->head);
}

/**
 * Subscribes the observer to the supplied subject.
 *
 * @param observer The observer to subscribe.
 * @param subject  The subject to subscribe the observer to.
 */
static inline void
observer_subscribe(struct observer *observer, struct subject *subject) {
    if (!dequeue_previous(&subject->observers) ||
        !dequeue_next(&subject->observers)) {
        dequeue_init(&subject->observers);
    }
    dequeue_insert_front(&subject->observers, &observer->head);
}

/** @brief Unsubscribes the observer from its subject. */
static inline void observer_unsubscribe(struct observer *observer) {
    dequeue_remove(&observer->head);
}

/** @brief Unsubscribes all observers from the given subject. */
static inline void subject_unsubscribe_all(struct subject *subject) {
    if (!subject->notifier || !dequeue_previous(&subject->observers) ||
        !dequeue_next(&subject->observers)) {
        return;
    }

    struct observer *current, *tmp;
    dequeue_for_each_entry_safe (current, tmp, &subject->observers, head) {
        observer_unsubscribe(current);
    }
}

#endif
