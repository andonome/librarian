/* SPDX-License-Identifier: Apache-2.0 */

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int strcmp_case(const char *s1, const char *s2) {
    if (!s1 && !s2) { return 0; }
    if (!s1) { return -1; }
    if (!s2) { return 1; }

    int t = 0;
    while ((t = (*s1 && *s2 && (tolower(*s1++) - tolower(*s2++) == 0)))) { }
    return sgn(t);
}

int strcmp_null_safe(const char *s1, const char *s2) {
    return strcmp(s1 ? s1 : "", s2 ? s2 : "");
}

void strip_whitespaces(char *str) {
    char *end = str + strlen(str) - 1;
    while (isspace(*end) && end >= str) { end--; }
    end++;

    // All spaces?
    if (end == str) {
        *end = '\0';
        return;
    }

    char *start = str;
    while (*start && isspace(*start)) { start++; }
    size_t chars = end - start;
    memmove(str, start, chars);
    str[chars] = '\0';
}

bool string_is_null_or_empty(const char *str) {
    if (!str) { return true; }

    while (*str && isspace(*str)) { str++; }

    return !*str;
}

int str_to_int(const char *str, long int *val) {
    char *end;

    *val = strtol(str, &end, 10);
    if (*str == 0 || *end != 0) { return -1; }
    return 0;
}

static const char * const TERM_COLOR_NAMES[1 + 8 * 2 + 1] = { "default",
    "black", "red", "green", "yellow", "blue", "magenta", "cyan", "gray",
    "darkgray", "lightred", "lightgreen", "lightyellow", "lightblue",
    "lightmagenta", "lightcyan", "white", NULL };

int str_to_term_color(const char *str) {
    long int tmp;
    if (str_to_int(str, &tmp) == 0) {
        if (tmp < -1 || tmp > 255) { return -2; }
        return tmp;
    }

    if (strcmp(str, *TERM_COLOR_NAMES) == 0) { return -1; }

    for (int i = 1; TERM_COLOR_NAMES[i]; i++) {
        if (strcmp(TERM_COLOR_NAMES[i], str) == 0) { return i - 1; }
    }

    return INVALID_TERM_COLOR;
}
