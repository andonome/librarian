/* SPDX-License-Identifier: Apache-2.0 */

#include <ctype.h>
#include <errno.h>
#include <ftw.h>
#include <libgen.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wordexp.h>

#include "core.h"
#include "file.h"
#include "limits.h"
#include "localization.h"
#include "log.h"
#include "types.h"
#include "utils.h"
#include "xmalloc.h"

void path_set_c_str(struct path *path, const char *c_str, size_t length) {
    u_string_set_c_str_n(&path->str, c_str, length);
}

void path_cleanup(struct path *path) { u_string_cleanup(&path->str); }

int path_compare(const struct path *lhs, const struct path *rhs) {
    return u_string_compare(&lhs->str, &rhs->str);
}

void path_copy(struct path *destination, const struct path *source) {
    u_string_set(&destination->str, &source->str);
}

bool path_expand(struct path *path) {
    wordexp_t p;
    int status = wordexp(path->str.c_str, &p, 0);

    // https://www.gnu.org/software/libc/manual/html_node/Calling-Wordexp.html
    if (status != 0) { return false; }

    size_t length = 0;
    for (size_t i = 0; i < p.we_wordc; i++) { length += strlen(p.we_wordv[i]); }

    size_t new_capacity = max(length + 1, path->str.capacity);
    char *tmp_path = xnew_n(char, new_capacity);

    char *it = tmp_path;
    for (size_t i = 0; i < p.we_wordc; i++) {
        size_t wlen = strlen(p.we_wordv[i]);
        strcpy(it, p.we_wordv[i]);
        it += wlen;
    }

    if (new_capacity > path->str.capacity) {
        path->str.c_str = xrenew(char, path->str.c_str, new_capacity);
    }

    strncpy(path->str.c_str, tmp_path, length + 1);
    path->str.capacity = new_capacity;
    path->str.byte_length = length;
    path->str.length = u8_strlen(path->str.c_str);

    wordfree(&p);
    xfree(tmp_path);

    return true;
}

bool path_real(struct path *path) {
    char *tmp = xnew_n(char, PATH_MAX + 1);
    char *result = realpath(path->str.c_str, tmp);
    if (result != tmp) { return false; }
    u_string_set_c_str(&path->str, tmp);
    xfree(tmp);
    return true;
}

bool path_is_directory(const struct path *path) {
    struct stat st;
    if (stat(path->str.c_str, &st)) { return false; }
    return S_ISDIR(st.st_mode);
}

bool path_is_file(const struct path *path) {
    struct stat st;
    if (stat(path->str.c_str, &st)) { return false; }
    return S_ISREG(st.st_mode);
}

void path_append(struct path *path, const char *c_str) {
    u_string_append_c_str(&path->str, c_str, strlen(c_str));
}

void path_basename(const struct path *path, const char **suffix) {
    *suffix = basename(path->str.c_str);
}

bool path_create_file(const struct path *path, const char *initial_content) {
    FILE *fp = fopen(path->str.c_str, "w");
    if (!fp) { return false; }
    fprintf(fp, "%s", initial_content);
    fclose(fp);
    return true;
}

bool path_create_directory(const struct path *path) {
    register const char sep = LIBRARIAN_SYSTEM_PATH_SEPARATOR[0];
    for (char *p = strchr(path->str.c_str + 1, sep); p;
         p = strchr(p + 1, sep)) {
        *p = '\0';
        if (mkdir(path->str.c_str, 0755) && errno != EEXIST) { return false; }
        *p = sep;
    }
    return true;
}

#define NFTW_USE_FDS 10

_Thread_local void (*_callback)(const struct path *);

static int __walk_directory_impl(const char *filename, const struct stat *info,
    const int typeflag, struct FTW *pathinfo) {

    if (typeflag == FTW_F) {
        struct path tmp = PATH_INIT;
        path_set_c_str(&tmp, filename, info->st_size);
        _callback(&tmp);
        path_cleanup(&tmp);
    }

    return 0;
}

void walk_directory(const struct path *path,
    void (*callback)(const struct path *path)) {
    _callback = callback;
    nftw(path->str.c_str, __walk_directory_impl, NFTW_USE_FDS, 0);
}

bool path_has_suffix(const struct path *path, const char *suffix) {
    return c_str_has_suffix(path->str.c_str, path->str.byte_length, suffix,
        strlen(suffix));
}
