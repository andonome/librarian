/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LIMITS_H__
#define __LIBRARIAN_LIMITS_H__

#ifdef __linux__
#include <linux/limits.h>
#elif defined(__APPLE__) && defined(__MACH__)
#include <sys/syslimits.h>
#else
#define PATH_MAX 1024
#endif

#endif
