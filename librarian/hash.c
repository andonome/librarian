/* SPDX-License-Identifier: Apache-2.0 */

#include "hash.h"
#include "types.h"

hash hash_str_djb2(const void *ptr) {
    hash val = 5381;
    for (const char *str = ptr; *str; str++) { val = val * 33 ^ *str; }
    return val;
}
