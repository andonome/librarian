/* SPDX-License-Identifier: Apache-2.0 */

#include <stdlib.h>

#include "command.h"
#include "config.h"
#include "core.h"
#include "format.h"
#include "library.h"
#include "localization.h"
#include "observer.h"
#include "plugin-registry.h"

static void _librarian_init_notifier(struct observer *observer) {
    ((void (*)()) observer->notify)();
}

static struct lifecycle_exit_args _exit_args = { .status = 0 };

static void _librarian_exit_notifier(struct observer *observer,
    struct lifecycle_exit_args *args) {
    ((void (*)(struct lifecycle_exit_args *)) observer->notify)(args);
}

struct lifecycle lifecycle;

void librarian_init(void) {
    lifecycle.on_init = SUBJECT_INIT(_librarian_init_notifier);
    lifecycle.on_exit = SUBJECT_INIT(_librarian_exit_notifier);
    plugin_registry_init();
    locale_init();
    format_registry_init();
    config_init();
    library_init();
    library_load();
    subject_notify_observers(&lifecycle.on_init, NULL);
}

void librarian_exit(int status) {
    _exit_args.status = status;
    subject_notify_observers(&lifecycle.on_exit, &_exit_args);
    library_cleanup();
    config_cleanup();
    plugin_registry_cleanup();
    exit(status);
}
