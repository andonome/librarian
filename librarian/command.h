/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_COMMAND_H__
#define __LIBRARIAN_COMMAND_H__

#include "types.h"
#include "u-string.h"
#include "vector.h"

declare_vector_type_val(token, char);

struct command_parser {
    char *tokenizer_state;
    struct u_string buffer;
    struct vector token;
    struct vector commands;
};

struct command {
    const char *name;
    void (*init)(struct command *self);
    bool (*execute)(struct command_parser *parser);
    void (*cleanup)(struct command *self);
};

/** @related command_parser
 * @{
 */

void command_parser_init(struct command_parser *parser);

void command_parser_cleanup(struct command_parser *parser);

void command_parser_register_command(struct command_parser *parser,
    struct command *command);

bool command_parser_execute(struct command_parser *parser,
    const char *expression);

bool command_parser_token_matches(const struct command_parser *parser,
    const char *c_str);

bool command_parser_identifier(struct command_parser *parser);

bool command_parser_string(struct command_parser *parser);

bool command_parser_symbol(struct command_parser *parser, const char *symbol);

bool command_parser_end(struct command_parser *parser);

/** @} */

void commands_core_register(struct command_parser *parser);

/** @related command
 * @{
 */
declare_vector_type(command, struct command);

void command_init(struct command *command);

void command_cleanup(struct command *command);

/** @} */

#endif
