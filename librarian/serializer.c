/* SPDX-License-Identifier: Apache-2.0 */

#include <assert.h>

#include "serializer.h"
#include "utils.h"

void serializer_init(struct serializer *sz, FILE *file, bool big_endian) {
    sz->file = file;
    sz->big_endian = big_endian;
    // sz->file_end = fseek(file, 0, SEEK_END);
    // rewind(file);
}

bool serializer_write_int(struct serializer *sz, size_t size, intmax_t value) {
#ifdef BIG_ENDIAN
#define uses_big_endian __builtin_expect(sz->big_endian, true)
#else
#define uses_big_endian __builtin_expect(sz->big_endian, false)
#endif

    if (uses_big_endian) {
        char *bytes = (char *) &value;
        const size_t end = size - 1;

        for (size_t i = 0; i < size / 2; i++) {
            swap(bytes[i], bytes[end - 1]);
        }
    }

    size_t n = fwrite(&value, size, 1, sz->file);
    return n != 0;

#undef uses_big_endian
}

bool serializer_write_string(struct serializer *sz, char *str, size_t length) {
    bool success = serializer_write_int(sz, sizeof(size_t), length);
    if (!success) { return success; }

    size_t n = fwrite(str, 1, length, sz->file);
    return n == length;
}

bool serializer_read_int(struct serializer *sz, size_t size, void *value) {
    size_t n = fread(value, size, 1, sz->file);

    if (n == 0) { return false; }

    if (!sz->big_endian) { return true; }

    char *bytes = (char *) value;
    const size_t end = size - 1;

    for (size_t i = 0; i < size / 2; i++) { swap(bytes[i], bytes[end - i]); }

    return true;
}

bool serializer_write_bool(struct serializer *sz, bool value) {
    return serializer_write_int(sz, sizeof(bool), value);
}

bool serializer_read_bool(struct serializer *sz, bool *value) {
    return serializer_read_int(sz, sizeof(bool), value);
}

size_t
serializer_read_string(struct serializer *sz, char *str, size_t capacity) {
    size_t length;
    bool success = serializer_read_int(sz, sizeof(length), &length);
    if (!success) { return -1; }
    assert(length < capacity);
    size_t n = fread(str, sizeof(char), length, sz->file);

    if (n != length) { return -1; }

    str[length] = '\0';
    return length;
}

bool serializer_has_more(struct serializer *sz) {
    return !feof(sz->file);
    // return ftell(sz->file) == sz->file_end;
}
