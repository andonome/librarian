/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_FORMAT_H__
#define __LIBRARIAN_FORMAT_H__

#include "types.h"

struct format_provider {
    bool (*check)(const char *file, bool *success);
    struct document *(*load)(const char *file);
};

void format_registry_init(void);

void format_registry_cleanup(void);

void format_registry_add(struct format_provider *provider);

struct format_provider *format_registry_detect(const char *path);

#endif
