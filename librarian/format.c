/* SPDX-License-Identifier: Apache-2.0 */

#include "format.h"

#include "compiler.h"
#include "vector.h"

declare_vector_type_val(format_provider, struct format_provider *);

struct vector formats;

void format_registry_init(void) {
    format_provider_vector_init(&formats, 7, 3, 0.41);
}

void format_registry_cleanup(void) { format_provider_vector_cleanup(&formats); }

void format_registry_add(struct format_provider *provider) {
    format_provider_vector_push_back_val(&formats, provider);
}

struct format_provider *format_registry_detect(const char *path) {
    bool success = false;
    struct format_provider **curr;
    vector_for_each_entry (curr, struct format_provider *, formats) {
        if (!(*curr)) { continue; }
        if ((*curr)->check(path, &success)) { return *curr; }
        // TODO: Handle success == false
    }
    return NULL;
}
