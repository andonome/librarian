/* SPDX-License-Identifier: Apache-2.0 */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "command.h"
#include "config.h"
#include "file.h"
#include "library.h"
#include "localization.h"
#include "log.h"
#include "plugin-registry.h"
#include "types.h"

void command_init(struct command *command) {
    command->init = NULL;
    command->execute = NULL;
    command->cleanup = NULL;
}

void command_cleanup(struct command *command) { command->cleanup(command); }

void command_parser_init(struct command_parser *parser) {
    parser->buffer = U_STRING_INIT;
    command_vector_init(&parser->commands, 10, 7, .41);
    token_vector_init(&parser->token, 10, 13, .72);
}

void command_parser_register_command(struct command_parser *parser,
    struct command *command) {
    command_vector_push_back(&parser->commands, command);
    if (command->init) { command->init(command); }
}

void command_parser_cleanup(struct command_parser *parser) {
    u_string_cleanup(&parser->buffer);
    struct command *cmd;
    vector_for_each_entry (cmd, struct command, parser->commands) {
        if (cmd->cleanup != NULL) { cmd->cleanup(cmd); }
    }

    command_vector_cleanup(&parser->commands);
    token_vector_cleanup(&parser->token);
}

static inline void
_command_parser_null_terminate(struct command_parser *parser) {
    // Ensure token is null terminated. This is ok because capacity will always
    // be at least 1 byte greater than size.
    ((char *) parser->token.data)[parser->token.size] = '\0';
}

bool command_parser_identifier(struct command_parser *parser) {
    char **curr = &parser->tokenizer_state;

    skip_spaces(curr);
    if (!**curr) { return false; }

    token_vector_clear(&parser->token);

    while (**curr && (isalnum(**curr) || **curr == '\\' || **curr == '_')) {
        if (**curr == '\\' && *(*curr + 1) != '\0') { (*curr)++; }
        token_vector_push_back_val(&parser->token, **curr);
        (*curr)++;
    }

    _command_parser_null_terminate(parser);

    return true;
}

bool command_parser_string(struct command_parser *parser) {
    char **curr = &parser->tokenizer_state;
    if (!*curr) { return false; }
    skip_spaces(curr);
    if (!**curr) { return false; }

    token_vector_clear(&parser->token);

    // TODO: Replace with something less restrictive
    if (**curr != '"') {
        while (**curr && isgraph(**curr)) {
            if (**curr == '\\' && *(*curr + 1) != '\0') { (*curr)++; }
            token_vector_push_back_val(&parser->token, **curr);
            (*curr)++;
        }
    } else {
        (*curr)++;
        while (**curr && **curr != '"') {
            if (**curr == '\\' && *(*curr + 1) != '\0') { (*curr)++; }
            token_vector_push_back_val(&parser->token, **curr);
            (*curr)++;
        }

        if (**curr != '"') {
            log_error(_("Unexpected end of line. Expected to read \"."));
            token_vector_clear(&parser->token);
            return false;
        }
        (*curr)++;
    }

    _command_parser_null_terminate(parser);

    return true;
}

bool command_parser_symbol(struct command_parser *parser, const char *symbol) {
    char **curr = &parser->tokenizer_state;
    if (!*curr) { return false; }
    skip_spaces(curr);
    if (!**curr) { return false; }

    while (**curr && **curr == *symbol) {
        (*curr)++;
        symbol++;
    }

    return *symbol == '\0';
}

bool command_parser_execute(struct command_parser *parser,
    const char *expression) {
    u_string_set_c_str(&parser->buffer, expression);
    parser->tokenizer_state = parser->buffer.c_str;

    skip_spaces(&parser->tokenizer_state);

    // Ignore empty statements and comments
    if (!parser->tokenizer_state || *parser->tokenizer_state == '"') {
        return true;
    }

    if (!command_parser_identifier(parser)) { return true; }

    bool success = false;

    struct command *cmd;
    vector_for_each_entry (cmd, struct command, parser->commands) {
        if (!cmd->execute) {
            log_warning("command '%s' has not implemented execute()",
                cmd->name);
            continue;
        }
        success |= cmd->execute(parser);
        if (success) { break; }
    }

    return success;
}

bool command_parser_token_matches(const struct command_parser *parser,
    const char *c_str) {
    return strcmp(parser->token.data, c_str) == 0;
}

bool command_parser_end(struct command_parser *parser) {
    char *it = parser->tokenizer_state;
    const char *end = it;
    bool has_trailing = false;

    skip_spaces(&it);

    while (*it != '\0') {
        has_trailing |= !isspace(*it);
        it++;
    }

    if (has_trailing) {
        char *message = _("Unexpected string \"%.*s\" after statement!");
        log_error(message, it - end, end);
    }

    return !has_trailing;
}

/* ---------------------- Core Commands Implementation ---------------------- */

bool command_add(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "add")) { return false; }

    bool success = command_parser_string(parser);

    if (!success) {
        log_error("%s", _("No path specified."));
        return true;
    }

    struct path tmp = PATH_INIT;
    path_set_c_str(&tmp, parser->token.data, parser->token.size);

    if (!command_parser_end(parser)) { goto cleanup; }

    if (path_expand(&tmp) && path_real(&tmp) && path_is_directory(&tmp)) {
        library_add_from_dir(&tmp);
    } else {
        log_error("%s", _("Invalid path specified."));
    }

cleanup:
    path_cleanup(&tmp);
    return true;
}

bool command_clear(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "clear")) { return false; }
    library_clear();
    return true;
}

static void _command_set_handle_errors(enum config_result result, char *key);

bool command_set(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "set")) { return false; }

    enum config_result result;
    char **curr = &parser->tokenizer_state;

    /* ? - zero or one */
    /* * - zero or more */
    /* set_option := set (<no>?<var> | <var><space>*=<space>*<value>)<eol> */

    bool success = command_parser_identifier(parser);

    if (!success) {
        log_error(_("Expected an identifier!"));
        return true;
    }

    char *key = NULL, *value = NULL;
    char *key_part = NULL;

    key = xnew_n(char, parser->token.size + 1);
    strncpy(key, parser->token.data, parser->token.size);
    key[parser->token.size] = '\0';
    key_part = key; // Changes to key + 2 in case no<option> is passed

    skip_spaces(curr);

    if (**curr == '=') {
        (*curr)++;
        success = command_parser_string(parser);

        if (!success) {
            log_error("%s", _("expected a value!"));
            goto cleanup;
        }

        value = xnew_n(char, parser->token.size + 1);
        strncpy(value, parser->token.data, parser->token.size);
        value[parser->token.size] = '\0';

        if (!command_parser_end(parser)) { goto cleanup; }

        result = config_set(key, value);
    } else {
        if (!command_parser_end(parser)) { goto cleanup; }

        bool val = strncmp("no", key, 2);

        key_part = val ? key : key + 2;

        const struct option *opt = config_find_option(key_part);

        if (!opt) {
            result = STATUS_NO_KEY;
        } else if (opt->type != BOOL_OPTION) {
            result = STATUS_NOT_A_BOOL;
        } else {
            result = config_set_bool(opt, val ? "true" : "false");
        }
    }

    _command_set_handle_errors(result, key_part);

cleanup:
    if (key != NULL) { xfree(key); }
    if (value != NULL) { xfree(value); }

    return true;
}

bool command_toggle(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "toggle")) { return false; }

    bool success = command_parser_identifier(parser);

    if (!success) {
        log_error(_("Expected an identifier!"));
        return true;
    }

    if (!command_parser_end(parser)) { return true; }

    char *id = parser->token.data;
    const struct option *opt = config_find_option(id);
    enum config_result result;

    if (opt == NULL) {
        result = STATUS_NO_KEY;
    } else if (opt->type != BOOL_OPTION) {
        result = STATUS_NOT_A_BOOL;
    } else {
        result = config_set_bool_val(opt,
            !*container_of(opt, struct bool_option, option)->value);
    }

    _command_set_handle_errors(result, id);

    return true;
}

static void _command_set_handle_errors(enum config_result result, char *key) {
    if (result == STATUS_OK) { return; }

    if (result == STATUS_NO_KEY) {
        // FIXME: How safe is this?
        log_error(_("Option \"%s\" does not exist!"), key);
        return;
    }

    if (result == STATUS_NOT_A_BOOL) {
        // FIXME: How safe is this?
        log_error(_("Option \"%s\" is not a boolean type!"), key);
        return;
    }

    if (result == STATUS_INVALID_INT) {
        log_error("%s", _("expected an integer value!"));
        return;
    }

    if (result == STATUS_INVALID_COLOR) {
        log_error("%s", _("color or integer in range -1 ... 255 expected!"));
        return;
    }

    if (result == STATUS_INVALID_BOOL) {
        log_error("%s", _("boolean value, 'true' or 'false' expected!"));
        return;
    }

    if (result == STATUS_INVALID_ENUM) {
        log_error("%s", _("invalid enum value!"));
        return;
    }
}

bool command_map(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "map")) { return false; }

    char *token = strtok_r(NULL, "", &parser->tokenizer_state);
    char *mnemonic = strtok_r(token, "", &parser->tokenizer_state);

    if (!mnemonic) {
        log_error("%s", _("Expected a mnemonic and a command string!"));
        return true;
    }

    size_t mn_len = mnemonic - token;
    while (isalnum(*token)) {
        mn_len++;
        token++;
    }
    skip_spaces(&token);
    token[mn_len + 1] = '\0';
    log_info("current: %s", token);
    return true;
}

bool command_load_plugin(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "load_plugin")) { return false; }

    if (!command_parser_string(parser)) {
        log_error("%s", _("Expected a plugin identifier or path!"));
        return true;
    }

    if (!command_parser_end(parser)) { return true; }

    struct plugin_load_details details;
    plugin_load_details_init(&details);
    plugin_registry_load(parser->token.data, &details);
    if (details.status == PLUGIN_LOAD_ALREADY_LOADED) {
        log_info("%s", details.message.c_str);
    } else if (details.status != PLUGIN_LOAD_OK) {
        log_error("%s", details.message.c_str);
    }
    plugin_load_details_cleanup(&details);

    return true;
}

bool command_unload_plugin(struct command_parser *parser) {
    if (!command_parser_token_matches(parser, "unload_plugin")) {
        return false;
    }

    if (!command_parser_identifier(parser)) {
        log_error("%s", _("Expected an identifier!"));
        return true;
    }

    if (!command_parser_end(parser)) { return true; }

    enum plugin_unload_status status =
        plugin_registry_unload(parser->token.data);

    if (status == PLUGIN_UNLOAD_NOT_FOUND) {
        log_error(_("Plugin \"%s\" is not loaded."), parser->token.data);
    } else if (status == PLUGIN_UNLOAD_REFERENCED) {
        log_error(_("Plugin \"%s\" cannot be unloaded. Some other plugin is "
                    "using it."),
            parser->token.data);
    }

    return true;
}

bool command_quit(struct command_parser *parser) {
    if (command_parser_token_matches(parser, "q") ||
        command_parser_token_matches(parser, "q!") ||
        command_parser_token_matches(parser, "quit")) {
        librarian_exit(EXIT_SUCCESS);
    }
    return false;
}

#define COMMAND_LIST(CMD)                                                      \
    CMD("add", command_add)                                                    \
    CMD("clear", command_clear)                                                \
    CMD("set", command_set)                                                    \
    CMD("toggle", command_toggle)                                              \
    CMD("map", command_map)                                                    \
    CMD("load_plugin", command_load_plugin)                                    \
    CMD("unload_plugin", command_unload_plugin)                                \
    CMD("quit", command_quit)

void commands_core_register(struct command_parser *parser) {
#define COMMAND_MAKE_NEW(NAME, EXECUTE)                                        \
    do {                                                                       \
        struct command __cmd;                                                  \
        command_init(&__cmd);                                                  \
        __cmd.name = (NAME);                                                   \
        __cmd.execute = (EXECUTE);                                             \
        __cmd.cleanup = NULL;                                                  \
        command_parser_register_command(parser, &__cmd);                       \
    } while (false);

    COMMAND_LIST(COMMAND_MAKE_NEW);

#undef COMMAND_MAKE_NEW
}

#undef COMMAND_LIST
