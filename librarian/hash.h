/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_HASH_H__
#define __LIBRARIAN_HASH_H__

#include "types.h" // hash

hash hash_str_djb2(const void *ptr);

// Idea for an integer hash function: __v ^ (__v >> 32)

#endif
