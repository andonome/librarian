/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_DOCUMENT_TYPES_H__
#define __LIBRARIAN_DOCUMENT_TYPES_H__

#include "dequeue.h"
#include "document.h"
#include "vector.h"

declare_vector_type_val(document, struct document *);

struct document_list {
    struct document *doc;
    struct dequeue_head head;
};

static inline struct document_list *document_list_new(struct document *doc) {
    struct document_list *node = xnew(struct document_list);
    node->head = (struct dequeue_head) DEQUEUE_HEAD_INIT(node->head);
    node->doc = doc;
    return node;
}

static inline void document_list_destroy(struct document_list **doc) {
    xfree_ptr(doc);
}

#endif
