/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_FILE_H__
#define __LIBRARIAN_FILE_H__

#include <errno.h>
#include <stdlib.h>

#include "limits.h"
#include "types.h"
#include "u-string.h"

#ifndef LIBRARIAN_SYSTEM_PATH_SEPARATOR
#define LIBRARIAN_SYSTEM_PATH_SEPARATOR "/"
#endif

struct path {
    struct u_string str;
};

#define PATH_INIT ((struct path){ .str = U_STRING_INIT_STATIC })

void path_set_c_str(struct path *path, const char *c_str, size_t length);

void path_cleanup(struct path *path);

int path_compare(const struct path *lhs, const struct path *rhs);

void path_copy(struct path *destination, const struct path *source);

bool path_expand(struct path *path);

bool path_real(struct path *path);

bool path_is_directory(const struct path *path);

bool path_is_file(const struct path *path);

void path_append(struct path *path, const char *c_str);

/**
 * Returns the basename of the provided path.
 *
 * @param path
 * @param suffix[out] a pointer which will be set to the beginning of the
 * basename suffix.
 */
void path_basename(const struct path *path, const char **suffix);

bool path_create_file(const struct path *path, const char *initial_content);

/**
 * Recursively creates directories from path.
 *
 * @return a boolean value indicating if creation was successful or not.
 * @remark If the directory exists this function will still return true.
 */
bool path_create_directory(const struct path *path);

bool path_has_suffix(const struct path *path, const char *suffix);

void walk_directory(const struct path *path,
    void (*callback)(const struct path *path));

#endif
