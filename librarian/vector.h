/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_VECTOR_H__
#define __LIBRARIAN_VECTOR_H__

#include <assert.h>
#include <memory.h>
#include <stdlib.h>

#include "compiler.h"
#include "types.h"
#include "xmalloc.h"

struct vector {
    size_t size;
    size_t element_size;
    size_t capacity;
    size_t threshold;
    float scale_factor;
    void *data;
};

/**
 * Initializes the vector structure.
 *
 * @param vec Vector container.
 * @param element_size Size of the vector elements.
 * @param capacity Desired capacity.
 * @param threshold Threshold at which the container tries to resize.
 * @related vector
 */
static inline void vector_init(struct vector *vec, size_t element_size,
    size_t capacity, size_t threshold, float scale_factor) {
    // TODO: Check capacity
    // TODO: Check threshold
    // TODO: Check scale factor
    vec->size = 0;
    vec->element_size = element_size;
    vec->capacity = capacity;
    vec->threshold = threshold;
    vec->scale_factor = scale_factor;
    assert(threshold > 0);
    vec->data = xmalloc(capacity * vec->element_size);
    memset(vec->data, 0, capacity * vec->element_size);
}

/**
 * releases the underlying buffer.
 *
 * @related vector
 */
static inline void vector_cleanup(struct vector *vec) { xfree_ptr(&vec->data); }

/**
 * clear the vector container.
 *
 * @memberof vector
 * @bug might not have 7 elements!
 */
static inline void vector_clear(struct vector *vec) {
    vec->size = 0;
    vec->threshold = 7; // TODO: Reorganize this!
    vec->capacity = 10;
}

/**
 * Tests if the vector is empty.
 * @memberof vector
 */
static inline bool vector_empty(struct vector *vec) { return vec->size == 0; }

/**
 * Returns the element at nth position.
 *
 * @param vec Vector container.
 * @param n Index of the requested element.
 * @return a pointer to the n-th element in the vector.
 * @memberof vector
 */
static inline void *vector_at(const struct vector *vec, size_t n) {
    return (uint8_t *) vec->data + n * vec->element_size;
}

/**
 * Returns the first element.
 *
 * @param vec Vector container.
 * @return a pointer to the first element in the vector.
 * @memberof vector
 */
static inline void *vector_front(const struct vector *vec) {
    return (uint8_t *) vec->data;
}

/**
 * Returns the last element.
 *
 * @param vec Vector container.
 * @return a pointer to the last element in the vector.
 * @memberof vector
 */
static inline void *vector_back(const struct vector *vec) {
    return (uint8_t *) vec->data + (vec->size - 1) * vec->element_size;
}

/**
 * Internally used to grow the vector container.
 *
 * @private
 * @memberof vector
 */
static inline void _vector_grow(struct vector *vec) {
    size_t new_capacity =
        vec->capacity + 1 + (size_t) (vec->scale_factor * vec->capacity);
    assert(new_capacity > vec->capacity);
    vec->data = xrealloc(vec->data, new_capacity * vec->element_size);
    vec->capacity = new_capacity;
    vec->threshold += (size_t) (vec->threshold * vec->scale_factor);
}

/**
 * Push the provided element at the end of the vector.
 * @memberof vector
 */
static inline void vector_push_back(struct vector *vec, void *data) {
    if (vec->size >= vec->threshold) { _vector_grow(vec); }
    void *begin = vector_at(vec, vec->size);
    memcpy(begin, data, vec->element_size);
    vec->size++;
}

/**
 * Pop an element from the end of the vector.
 *
 * @param[out] data Pointer to a buffer where the requested element will be
 * copied if it is not NULL.
 * @memberof vector
 */
static inline void vector_pop_back(struct vector *vec, void *data) {
    // TODO: Handle empty vector error (vec->size == 0)
    vec->size--;
    if (data != NULL) {
        void *begin = vector_at(vec, vec->size);
        memcpy(data, begin, vec->element_size);
    }
}

/**
 * Insert an element at the desired position.
 *
 * @memberof vector
 */
static inline void
vector_insert(struct vector *vec, size_t position, void *data) {
    // TODO: Handle out of capacity error (position >= vec->capacity)
    if (vec->size >= vec->threshold) { _vector_grow(vec); }
    if (position < vec->size) {
        const size_t diff = (vec->size - position) * vec->element_size;
        memmove(vector_at(vec, position + 1), vector_at(vec, position), diff);
        vec->size++;
    } else {
        vec->size = position + 1;
    }
    memcpy(vector_at(vec, position), data, vec->element_size);
}

/**
 * Erase an element from the provided position.
 *
 * @param vec Vector container.
 * @param position Index of the element to be erased.
 * @memberof vector
 */
static inline void vector_erase(struct vector *vec, size_t position) {
    // TODO: Handle empty array error (vec->size == 0)
    // TODO: Handle out of bounds error (position >= vec->size)
    const size_t diff = (vec->size - 1 - position) * vec->element_size;
    memmove(vector_at(vec, position), vector_at(vec, position + 1), diff);
    vec->size--;
}

/* ------------------------ Type-specific functions  ------------------------ */

/**
 * Declares a type-specific interface that wraps the generic vector functions.
 *
 * @param NAME The prefix of the new type-specific vector functions (e.g. if
 * NAME is document then the generated interfaces will start with document_.
 * @param TYPE The type of the contained element.
 * @related vector
 */
#define declare_vector_type(NAME, TYPE)                                        \
                                                                               \
    attr_unused static inline void NAME##_vector_init(struct vector *vec,      \
        size_t capacity, size_t threshold, float scale_factor) {               \
        vector_init(vec, sizeof(TYPE), capacity, threshold, scale_factor);     \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_cleanup(struct vector *vec) { \
        vector_cleanup(vec);                                                   \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_clear(struct vector *vec) {   \
        vector_clear(vec);                                                     \
    }                                                                          \
                                                                               \
    attr_unused static inline bool NAME##_vector_empty(struct vector *vec) {   \
        return vector_empty(vec);                                              \
    }                                                                          \
                                                                               \
    attr_unused static inline TYPE *NAME##_vector_at(const struct vector *vec, \
        size_t n) {                                                            \
        return (TYPE *) vector_at(vec, n);                                     \
    }                                                                          \
                                                                               \
    attr_unused static inline TYPE *NAME##_vector_back(                        \
        const struct vector *vec) {                                            \
        return (TYPE *) vector_back(vec);                                      \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_push_back(struct vector *vec, \
        TYPE *element) {                                                       \
        vector_push_back(vec, element);                                        \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_pop_back(struct vector *vec,  \
        TYPE *element) {                                                       \
        vector_pop_back(vec, element);                                         \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_insert(struct vector *vec,    \
        size_t position, TYPE *element) {                                      \
        vector_insert(vec, position, element);                                 \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_erase(struct vector *vec,     \
        size_t position) {                                                     \
        vector_erase(vec, position);                                           \
    }

/**
 * A variant of @ref declare_vector_type meant to be used for storing value
 * types.
 *
 * @related vector
 */
#define declare_vector_type_val(NAME, TYPE)                                    \
                                                                               \
    declare_vector_type(NAME, TYPE);                                           \
                                                                               \
    attr_unused static inline TYPE NAME##_vector_at_val(struct vector *vec,    \
        size_t position) {                                                     \
        return *(TYPE *) (vector_at(vec, position));                           \
    }                                                                          \
                                                                               \
    attr_unused static inline TYPE NAME##_vector_back_val(                     \
        struct vector *vec) {                                                  \
        return *(TYPE *) (vector_back(vec));                                   \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_push_back_val(                \
        struct vector *vec, TYPE element) {                                    \
        vector_push_back(vec, &element);                                       \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_insert_val(                   \
        struct vector *vec, size_t position, TYPE element) {                   \
        vector_insert(vec, position, &element);                                \
    }

/* ---------------------------- Iteration macros  --------------------------- */

/**
 * A construct for iterating over a vector.
 *
 * @param pos A (type *) that holds the current element.
 * @param type The type of elements the vector holds.
 * @param vector Vector container.
 * @related vector
 */
#define vector_for_each_entry(pos, type, vector)                               \
    for ((pos) = (type *) (vector).data;                                       \
         (pos) - (type *) (vector).data != (vector).size; ++(pos))

/**
 * A construct for iterating over a vector in reverse order.
 *
 * @param pos A (type *) that holds the current element.
 * @param type The type of elements the vector holds.
 * @param vector Vector container.
 * @related vector
 */
#define vector_for_each_entry_reverse(pos, type, vector)                       \
    for ((pos) = (type *) (vector).data + (vector).size - 1;                   \
         (pos) >= (type *) (vector).data; --(pos))

#endif
