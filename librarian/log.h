/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LOG_H__
#define __LIBRARIAN_LOG_H__

#include "types.h"

enum log_mode { INFO, WARN, ERROR };
typedef void (*log_flush)(const char * const buffer, enum log_mode mode);

#define LOG_BUFFER_SIZE 512

#define log_info(...) log_print(INFO, __VA_ARGS__)
#define log_warning(...) log_print(WARN, __VA_ARGS__)
#define log_error(...) log_print(ERROR, __VA_ARGS__)

void log_print(enum log_mode mode, const char *format, ...);

void log_set_flush(log_flush flush_handler);

extern bool log_is_empty;

#endif
