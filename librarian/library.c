/* SPDX-License-Identifier: Apache-2.0 */

#include <ftw.h>
#include <stdlib.h>

#include "config.h"
#include "core.h"
#include "dequeue.h"
#include "document-types.h"
#include "document.h"
#include "file.h"
#include "format.h"
#include "library.h"
#include "localization.h"
#include "log.h"
#include "observer.h"
#include "rbtree.h"
#include "serializer.h"
#include "u-string.h"
#include "vector.h"
#include "xmalloc.h"

struct library library = { .path = PATH_INIT, .lib_tree = RBTREE, .size = 0 };

_Thread_local size_t _new_docs_count;

struct vector _new_docs;

/* -------------------- Internal function declarations ---------------------- */

/**
 * This function is passed to @ref walk_directory in @ref library_add_from_dir.
 */
void _library_add_from_dir_impl(const struct path *path);

void _library_notify_observer(struct observer *observer,
    struct library_args *args);

void _library_notify_cleared(struct observer *observer);

void _library_write_headers(struct serializer *sz);

/**
 * Reads the library headers.
 *
 * @return true if headers are valid and successfully read, false otherwise.
 * @remark This is a good place to check whether the library file and current
 * version of librarian are compatible. If not, @ref library_load can be stopped
 * and appropriate error message can be issued. Alternatively, we can implement
 * migrations between library versions.
 */
bool _library_read_headers(struct serializer *sz);

/**
 * Internal function to free the library tree depth-first.
 *
 * @param rbnode Root node of the tree to be freed.
 */
void _library_free(struct rbnode **rbnode);

/* ---------------------------- Implementation ------------------------------ */

void library_init(void) {
    config_get_path(&library.path, "library");
    library.subject = SUBJECT_INIT(_library_notify_observer);
    library.cleared = SUBJECT_INIT(_library_notify_cleared);
    library.version = LIBRARIAN_VERSION;
    document_vector_init(&_new_docs, 10, 7, 0.41);
}

void library_load(void) {
    // BUG: Might be directory or sth else
    if (!path_is_file(&library.path)) { return; } // Not created yet

    FILE *fp = fopen(library.path.str.c_str, "rb");

    if (!fp) {
        log_warning("%s", _("Failed to open library file for reading."));
        return;
    }

    struct serializer sz;
    serializer_init(&sz, fp, false);

    bool success = _library_read_headers(&sz);

    if (!success) { goto exit; }

    while (serializer_has_more(&sz)) {
        struct document *doc = document_new();
        bool success = document_deserialize(doc, &sz);
        if (success && !library_contains_document(doc)) { library_add(doc); }
        document_unref(&doc);
    }
exit:
    fclose(fp);
}

bool _library_read_headers(struct serializer *sz) {
    bool success =
        serializer_read_int(sz, sizeof(lib_version), &library.version);

    if (!success) { return true; } // Library file is empty

    if (lib_version_significant(library.version) >
        lib_version_significant(LIBRARIAN_VERSION)) {
        /* XXX: We can issue an appropriate migration here later, but for now we
         * only show warning. */
        log_warning(_("The library version %llu is greater than librarian "
                      "version %llu. Consider updating librarian."),
            library.version, LIBRARIAN_VERSION);
        return false;
    }

    return true;
}

bool library_contains_document(struct document *doc) {
    struct rbnode **link = &library.lib_tree.root;
    while (*link) {
        struct document *curr = rbtree_entry(*link, struct document, tree_node);
        if (doc->path.str.byte_length == curr->path.str.byte_length &&
            path_compare(&doc->path, &curr->path) == 0) {
            return true;
        }
        link = document_compare_by_author(doc, curr) > 0 ? &(**link).right
                                                         : &(**link).left;
    }
    return false;
}

void library_add(struct document *doc) {
    struct rbnode **link = &library.lib_tree.root, *parent = NULL;

    while (*link) {
        parent = *link;
        struct document *curr =
            rbtree_entry(parent, struct document, tree_node);
        link = document_compare_by_author(doc, curr) > 0 ? &(**link).right
                                                         : &(**link).left;
    }

    rbnode_link(&doc->tree_node, parent, link);
    rbtree_insert_fixup(&library.lib_tree, &doc->tree_node);
    document_ref(doc);
    library.size++;
}

void library_add_from_dir(const struct path * const dir) {
    _new_docs_count = 0;

    walk_directory(dir, _library_add_from_dir_impl);

    if (_new_docs_count > 0) {
        // XXX: This is dirty as we are copying the data ptr. Fix this sometime
        struct library_args args = { .docs = _new_docs,
            .length = _new_docs_count };
        subject_notify_observers(&library.subject, &args);
    }

    struct document **doc;
    vector_for_each_entry (doc, struct document *, _new_docs) {
        document_unref(doc);
    }
    document_vector_clear(&_new_docs);
    library_save();
}

void _library_add_from_dir_impl(const struct path *path) {
    struct format_provider *fmt = format_registry_detect(path->str.c_str);
    if (!fmt) { return; }

    struct document *doc = fmt->load(path->str.c_str);
    if (!library_contains_document(doc)) {
        library_add(doc);
        document_vector_push_back_val(&_new_docs, doc);
        document_ref(doc);
        _new_docs_count++;
    }
    document_unref(&doc);
}

void _library_notify_observer(struct observer *observer,
    struct library_args *args) {
    ((void (*)(struct library_args *)) observer->notify)(args);
}

void _library_notify_cleared(struct observer *observer) {
    ((void (*)()) observer->notify)();
}

void library_save(void) {
    // Don't destroy old library file
    if (lib_version_significant(library.version) >
        lib_version_significant(LIBRARIAN_VERSION)) {
        return;
    }

    FILE *fp = fopen(library.path.str.c_str, "wb+");

    if (!fp) {
        log_warning("%s", _("Failed to open library file for writing."));
        return;
    }

    struct serializer sz;
    serializer_init(&sz, fp, false);

    _library_write_headers(&sz);

    if (library.size == 0) { goto exit; }

    struct dequeue_head head = DEQUEUE_HEAD_INIT(head);

    struct document_list *root = document_list_new(
        rbtree_entry(library.lib_tree.root, struct document, tree_node));

    dequeue_insert_back(&head, &root->head);

    while (!dequeue_empty(&head)) {
        struct dequeue_head *curr = head.next;
        struct document_list *dlist =
            container_of(curr, struct document_list, head);
        dequeue_remove(curr);

        document_serialize(dlist->doc, &sz);

        struct rbnode *left_child = dlist->doc->tree_node.left;
        struct rbnode *right_child = dlist->doc->tree_node.right;

        if (left_child) {
            struct document_list *dlist = document_list_new(
                container_of(left_child, struct document, tree_node));
            dequeue_insert_back(&head, &dlist->head);
        }

        if (right_child) {
            struct document_list *dlist = document_list_new(
                container_of(right_child, struct document, tree_node));
            dequeue_insert_back(&head, &dlist->head);
        }

        xfree(dlist);
    }

exit:
    fclose(fp);
}

void _library_write_headers(struct serializer *sz) {
    serializer_write_int(sz, sizeof(lib_version), LIBRARIAN_VERSION);
}

void _library_free(struct rbnode **rbnode) {
    if (!rbnode || !*rbnode) { return; }
    if ((*rbnode)->left) { _library_free(&(*rbnode)->left); }
    if ((*rbnode)->right) { _library_free(&(*rbnode)->right); }
    struct document *doc = rbtree_entry(*rbnode, struct document, tree_node);
    document_unref(&doc);
}

void library_clear(void) {
    _library_free(&library.lib_tree.root);
    library.lib_tree = RBTREE;
    library.size = 0;
    library_save();
    subject_notify_observers(&library.cleared, NULL);
}

void library_filter(struct library_view *view,
    bool (*predicate)(struct document *doc)) {
    struct document *doc;
    rbtree_for_each_entry (doc, &library.lib_tree, tree_node) {
        if (!predicate || (predicate && predicate(doc))) {
            document_vector_push_back_val(&view->view, doc);
            document_ref(doc);
        }
    }
    view->compare = document_compare_by_author;
}

void library_cleanup(void) {
    subject_unsubscribe_all(&library.subject);
    subject_unsubscribe_all(&library.cleared);
    document_vector_cleanup(&_new_docs);
    _library_free(&library.lib_tree.root);
    path_cleanup(&library.path);
}

static _Thread_local lib_compare _cmp_impl;

int _order_impl(const void *lhs, const void *rhs) {
    return _cmp_impl(*(struct document **) lhs, *(struct document **) rhs);
}

struct document **
library_view_find(struct library_view *view, struct document *doc) {
    if (doc == NULL) { return NULL; }
    _cmp_impl = view->compare;
    return bsearch(&doc, view->view.data, view->view.size,
        sizeof(struct document *), _order_impl);
}

void library_view_reorder(struct library_view *view, lib_compare cmp) {
    _cmp_impl = view->compare = cmp;
    qsort(view->view.data, view->view.size, sizeof(struct document **),
        _order_impl);
}

void library_view_cleanup(struct library_view *view) {
    struct document **doc;
    vector_for_each_entry (doc, struct document *, view->view) {
        document_unref(doc);
    }
    vector_cleanup(&view->view);
}
