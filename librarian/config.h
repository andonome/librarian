/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_CONFIG_H__
#define __LIBRARIAN_CONFIG_H__

#include "file.h"
#include "limits.h"
#include "observer.h"
#include "option.h"
#include "types.h"

#ifndef LIBRARIAN_CONFIG_PATH
#define LIBRARIAN_CONFIG_PATH "$HOME/.config/librarian/"
#endif

#define CONFIG_OPEN_COMMAND_FORMAT_SIZE (PATH_MAX)

#define CONFIG_ORDER_DEFAULT "author"

struct config {
    struct path path;
    struct subject on_change;
    struct subject on_color_change;

    struct int_option color_highlight_bg;
    struct int_option color_highlight_fg;
    struct int_option color_selection_bg;
    struct int_option color_selection_fg;
    struct int_option color_info_bg;
    struct int_option color_info_fg;
    struct int_option color_warning_bg;
    struct int_option color_warning_fg;
    struct int_option color_error_bg;
    struct int_option color_error_fg;
    struct int_option color_text_primary;
    struct bool_option show_line_numbers;
    struct string_option colorscheme;
    struct string_option command_line_token;
    struct enum_option default_view;
    struct string_option open_command_format;
    struct string_option runtime_path;
    struct enum_option order;
};

extern struct config config;

struct config_args {
    const struct option *option;
};

enum config_result {
    STATUS_OK,
    STATUS_NO_KEY,
    STATUS_NOT_A_BOOL,
    STATUS_INVALID_COLOR,
    STATUS_INVALID_INT,
    STATUS_INVALID_BOOL,
    STATUS_INVALID_ENUM
};

/**
 * @related config
 * @{
 */

void config_init(void);

void config_load(void);

/* ----------------------------- Config parsing ----------------------------- */

void config_parse(const char *buffer);

const struct option *config_find_option(const char *name);

/* ----------------------------- Option setters ----------------------------- */

enum config_result config_set(const char *key, const char *value);

enum config_result
config_set_color(const struct option *opt, const char *value);

enum config_result config_set_int(const struct option *opt, const char *value);

enum config_result config_set_bool_val(const struct option *opt, bool value);

enum config_result config_set_bool(const struct option *opt, const char *value);

enum config_result
config_set_string(const struct option *opt, const char *value);

enum config_result config_set_enum(const struct option *opt, const char *value);

void config_get_path(struct path *path, const char *relative);

void config_cleanup(void);

/**
 * @}
 */

#endif
