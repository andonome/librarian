/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_XMALLOC_H__
#define __LIBRARIAN_XMALLOC_H__

#include <stdio.h>
#include <stdlib.h>

void *xmalloc(size_t size);
void *xrealloc(void *ptr, size_t size);

char *xstrdup(const char *c_str);

#define xnew(type) (type *) xmalloc(sizeof(type))

#define xnew_n(type, n) (type *) xmalloc(sizeof(type) * (n))

#define xrenew(type, mem, n) (type *) xrealloc(mem, sizeof(type) * (n))

#define xfree(ptr) free(ptr)

#define xfree_ptr(ptr2ptr)                                                     \
    do {                                                                       \
        if (*(ptr2ptr) != NULL) {                                              \
            xfree(*(ptr2ptr));                                                 \
            *(ptr2ptr) = NULL;                                                 \
        }                                                                      \
    } while (0);

#endif
