/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LIBRARY_H__
#define __LIBRARIAN_LIBRARY_H__

#include "document.h"
#include "limits.h"
#include "observer.h"
#include "rbtree.h"
#include "u-string.h"
#include "vector.h"
#include "version.h"

struct library {
    struct path path;
    struct rbtree lib_tree;
    struct subject subject;
    struct subject cleared;
    lib_version version;
    int size;
};

struct library_args {
    struct vector docs;
    size_t length;
};

// clang-format off
typedef bool (*lib_filter)(const struct document *doc);
typedef int (*lib_compare)(const struct document *lhs,
                           const struct document *rhs);
// clang-format on

struct library_view {
    struct vector view;
    struct subject add;
    struct subject clear;
    lib_filter filter;
    lib_compare compare;
};

extern struct library library;

/** @related library
 * @{
 */

void library_init(void);

void library_load(void);

/**
 * Tests if the library contains a document.
 */
bool library_contains_document(struct document *doc);

/**
 * Adds the document into the library.
 */
void library_add(struct document *doc);

/**
 * Recursively adds new documents from a directory.
 *
 * This functions walks the directory tree, adds new documents and writes their
 * paths to the library file.
 *
 * @param dir Directory path.
 * @remark Document will be added to the library only if its path is unique.
 */
void library_add_from_dir(const struct path *dir);

/**
 * Saves the library.
 *
 * This function performs a level-order traversal of the library and serializes
 * documents. This way when the library is loaded no rotations will be required.
 */
void library_save(void);

/**
 * Clears the library and saves the library file.
 *
 * @remark The library.cleared observers will be notified afterwards.
 */
void library_clear(void);

/**
 * @brief Traverses the library and adds the documents that satisfy the
 * predicate to the view.
 *
 * @param[out] view library_view to be populated.
 * @param predicate a predicate on a document.
 */
void library_filter(struct library_view *view,
    bool (*predicate)(struct document *doc));

/**
 * Clears the library and releases all the references to the contained
 * documents.
 *
 * @remark Observers of library.cleared will be notified afterwards.
 */
void library_cleanup(void);

/** @} */

/**
 * @memberof library_view
 */
struct document **
library_view_find(struct library_view *view, struct document *doc);

/**
 * Sets an order on a library_view.
 * @memberof library_view
 */
void library_view_reorder(struct library_view *view, lib_compare cmp);

/**
 * @memberof library_view
 */
void library_view_cleanup(struct library_view *view);

#endif
