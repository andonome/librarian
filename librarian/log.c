/* SPDX-License-Identifier: Apache-2.0 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "localization.h"
#include "log.h"

char log_buffer[LOG_BUFFER_SIZE];
bool log_is_empty = true;

static void
_default_flush_handler(const char * const buffer, enum log_mode mode) {
    FILE *out = stdout;
    if (mode == ERROR) {
        log_is_empty = false;
        out = stderr;
    }
    fprintf(out, "%s\n", buffer);
}

static log_flush _flush_handler = _default_flush_handler;

void log_print(enum log_mode mode, const char *format, ...) {
    va_list ap;
    va_start(ap, format);

    if (mode == WARN) {
        sprintf(log_buffer, "%s: ", _("Warning"));
    } else if (mode == ERROR) {
        sprintf(log_buffer, "%s: ", _("Error"));
    }

    char *end = (char *) log_buffer + strlen(log_buffer);

    vsnprintf(end, LOG_BUFFER_SIZE, format, ap);

    va_end(ap);

    _flush_handler(log_buffer, mode);

    log_buffer[0] = '\0';
}

void log_set_flush(log_flush flush_handler) { _flush_handler = flush_handler; }
