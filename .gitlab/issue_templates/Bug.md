### Basic information

**Operating system:** e.g. macOS 12.4 Monterey

**Librarian version:** X.X.X

### Expected behavior
Describe the expected behavior here.

### Actual behavior
Describe what actually happens

### Steps to reproduce the behavior

1. Step 1.
2. Step 2.
3. Step 3.

### Extra information

Here include things like (if available):
- Log files
- Screenshots
- Core Dump
