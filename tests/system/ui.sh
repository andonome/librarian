#!/bin/bash

source "$(dirname ${BASH_SOURCE[0]})/test-lib.sh"

set -e

readonly TEST_DOCS_DIR="/tmp/librarian-test"
readonly TEST_STATUS="/tmp/librarian-test-status"
readonly LIBRARIAN_CMD=${LIBRARIAN_CMD:-librarian}
readonly LIBRARIAN_HOOK="bash -c '${LIBRARIAN_CMD}; echo \$? > ${TEST_STATUS}'"

require "${LIBRARIAN_CMD}"

ui_setup() {
    tmux_start
    mkfifo -m 755 "${TEST_STATUS}"
    local dd="$1_MAKE_DUMMY_DOCS"
    if [ -n "${!dd}" ]; then create_pdf_test_dir "${TEST_DOCS_DIR}" "${!dd}"; fi
}

ui_teardown() {
    tmux_end
    rm -f "${TEST_STATUS}"
    local dd="$1_MAKE_DUMMY_DOCS"
    if [ -n "${!dd}" ]; then rm -rf "${TEST_DOCS_DIR}"; fi
}

ui_start_exit() {
    tmux_send                          \
        "${LIBRARIAN_HOOK}"      ENTER \
        :q                       ENTER
    return "$(cat ${TEST_STATUS})"
}

ui_clear_library() {
    tmux_send                          \
        "${LIBRARIAN_HOOK}"      ENTER \
        :clear                   ENTER \
        :q                       ENTER
    return "$(cat ${TEST_STATUS})"
}

ui_add_documents_MAKE_DUMMY_DOCS=500

ui_add_documents() {
    tmux_send                          \
        "${LIBRARIAN_HOOK}"      ENTER \
        :clear                   ENTER \
        ":add ${TEST_DOCS_DIR}"  ENTER \
        :q                       ENTER
    return "$(cat ${TEST_STATUS})"
}

ui_list_view_navigation_MAKE_DUMMY_DOCS=200

ui_list_view_navigation() {
    tmux_send                          \
        "${LIBRARIAN_HOOK}"      ENTER \
        :clear                   ENTER \
        ":add ${TEST_DOCS_DIR}"  ENTER \
        G :20                    ENTER \
        gg                             \
        :q                       ENTER
    return "$(cat ${TEST_STATUS})"
}

register_suite ui
register_suite_test ui ui_start_exit
register_suite_test ui ui_clear_library
register_suite_test ui ui_add_documents
register_suite_test ui ui_list_view_navigation

test_runner "$(basename $0)" $@
