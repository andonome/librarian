set(LIBRARIAN_RECOMMENDED_C_FLAGS "-Wall -Werror -Wpointer-arith"
    CACHE STRING "Recommended compilation flags for the C programming language."
)
mark_as_advanced(LIBRARIAN_RECOMMENDED_C_FLAGS)

set(LIBRARIAN_RECOMMENDED_LINK_FLAGS   "--gc-sections --print-gc-sections -flto"
    CACHE STRING "Recommended linker flags for the C programming language."
)
mark_as_advanced(LIBRARIAN_RECOMMENDED_LINK_FLAGS)

set(LIBRARIAN_LOCALIZATION_DEPS
    CACHE STRING "Localization Dependencies"
)
mark_as_advanced(LIBRARIAN_LOCALIZATION_DEPS)

# ----------------------------- Enable clang-tidy ---------------------------- #

find_program(CLANG_TIDY clang-tidy)
# find_program(IWYU iwyu)

if (NOT "${CLANG_TIDY}" STREQUAL "CLANG_TIDY-NOTFOUND")
    set(CMAKE_C_CLANG_TIDY ${CLANG_TIDY})
endif()

# if (NOT "${IWYU}" STREQUAL "IWYU-NOTFOUND")
#     set(CMAKE_C_INCLUDE_WHAT_YOU_USE ${IWYU} --transitive_includes_only)
# endif()

set(CMAKE_LINK_WHAT_YOU_USE TRUE)

# ---------------------- Documentation and Localization ---------------------- #

if (ENABLE_LOCALIZATION)
    if (APPLE)
        find_library(intl intl REQUIRED)
        list(APPEND LIBRARIAN_LOCALIZATION_DEPS intl)
    endif()
endif()

macro(translate_files)
    if (ENABLE_LOCALIZATION)
        set(TRANSLATE_SOURCES ${TRANSLATE_SOURCES} ${ARGV} PARENT_SCOPE)
    endif()
endmacro()

macro(doxygen_include_headers)
    if (ENABLE_DOCS)
        set(LIBRARIAN_DOXYGEN_HEADERS ${LIBRARIAN_DOXYGEN_HEADERS}
            ${ARGV} PARENT_SCOPE)
    endif()
endmacro()
