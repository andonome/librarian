add_subdirectory(pdf)
add_subdirectory(djvu)

if (ENABLE_EXPERIMENTAL_PLUGINS)
add_subdirectory(experimental)
endif()
