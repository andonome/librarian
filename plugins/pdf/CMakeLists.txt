cmake_minimum_required(VERSION 3.13)

project(pdf
    LANGUAGES C
    VERSION 0.0.1
    DESCRIPTION "Pdf format handling for librarian.")

set(CMAKE_C_STANDARD 11)
set(LIBRARIAN_CORE_LIBRARY_NAME librarian-core)

find_library(exempi exempi REQUIRED)

add_library(${PROJECT_NAME} SHARED pdf.c)

target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_SOURCE_DIR})

target_link_libraries(${PROJECT_NAME} ${LIBRARIAN_CORE_LIBRARY_NAME} exempi)

install(
    TARGETS ${PROJECT_NAME}
    DESTINATION share/librarian/plugins
)
