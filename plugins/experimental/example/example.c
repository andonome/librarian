/* SPDX-License-Identifier: Apache-2.0 */

#include <stdio.h>

#include <librarian/plugin.h>

exit_status example_load(void) {
    fprintf(stderr, "Hello from __FUNCTION_NAME__!");
    return 0;
}

exit_status example_unload(void) {
    fprintf(stderr, "Goodbye from __FUNCTION_NAME__!");
    return 0;
}

// Uncomment the following code to try out making a dependency to djvu plugin.

// const struct plugin_dependency example_dependency_djvu = {
//     .name = "djvu",
// };

// Pay attention that this array must be null-terminated!

// const struct plugin_dependency *example_plugin_dependencies[] = {
//     &example_dependency_djvu, NULL
// };

// clang-format off
struct plugin example = {
    .name = "Example Plugin",
    .maintainer = "Dušan Gvozdenović",
    .email = "dusan.gvozdenovic.99@gmail.com",
    .load = example_load,
    .unload = example_unload,
    .dependencies = NULL,
    // .dependencies = example_dependencies
};
// clang-format on

plugin_export(example);
