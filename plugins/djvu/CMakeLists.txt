cmake_minimum_required(VERSION 3.13)

project(djvu
    LANGUAGES C
    VERSION 0.0.1
    DESCRIPTION "djvu format handling for librarian.")

set(CMAKE_C_STANDARD 11)
set(LIBRARIAN_CORE_LIBRARY_NAME librarian-core)

find_library(exempi exempi REQUIRED)

add_library(${PROJECT_NAME} SHARED djvu.c)

target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_SOURCE_DIR})

target_link_libraries(${PROJECT_NAME} PRIVATE ${LIBRARIAN_CORE_LIBRARY_NAME})

install(
    TARGETS ${PROJECT_NAME}
    DESTINATION share/librarian/plugins
)
