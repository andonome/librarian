Plugin System {#plugin-system}
==============================

Librarian is extensible through a native plugin system that supports managing
dependencies.

A plugin consists of:
- `name` --- as seen by the user
- `maintainer` --- author's name
- `email` --- maintainer's email
- `website` --- URL leading to the plugin's website
- `license` --- license identifier
- `version` --- a number that represents the plugin's version in
  `major`.`minor`.`patch` format.
- `api_version` --- minimum required librarian API version.
- `dependencies` (optional). --- each dependency consists of:
    - `identifier` of the plugin to be loaded.
    - `min_version` (optional)
    - `max_version` (optional)

A `plugin's identifier` is determined by its library name and is required to
be unique at runtime. For example, given a library `libfoo.so` its plugin's
identifier is `foo`.

# Workflow

Plugins are loaded into librarian's main process and are such single threaded.
They are used to extend existing functionality while keeping the core minimal.
Threaded plugins will be addressed in the future.

<!-- TODO: Add diagrams -->

## Loading

### Name lookup

Invoking `load_plugin <name>` will perform a lookup in the following manner:

- If `<name>` starts with a system path separator (e.g. `/` on *NIX-es), it is
  treated as an absolute path and librarian will try to load it as such.
- If `<name>` has a suffix corresponding to the system's extension used for
  dynamically loadable libraries (e.g. `.so` on Linux or `.dylib` on macOS), it
  is treated as a relative path and will be looked up in the provided runtime
  path list.
- Otherwise, librarian will recursively search every directory specified in the
  runtime path list and search for a file named `lib<name>.so`.

A plugin is considered loaded if the plugin registry contains that plugin's
identifier.

### Procedure

- If the plugin is already loaded, do nothing.

A plugin is loaded in the following order:

- Shared libraries are opened.
- Dependencies and versions are checked.
- `plugin->load()` is called bottom-up.

## Unloading

- A plugin is unloaded by:
  - Decreasing a reference counter on all its dependencies (excluding loops).
  - Calling `plugin->unload()`.

A plugin can be unloaded only if no other plugin depends on it.

# References

- \ref plugin.h "plugin.h"
- \ref plugin-registry.h "plugin-registry.h"
