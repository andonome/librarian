Welcome to Librarian Documentation!        {#index}
===================================

Librarian is an ncurses-based program that helps you organize, tag, view and
search your documents. It aims to be as small and portable as possible but
also extendable.
Heavily inspired by [vim](https://www.vim.org) and [cmus](https://cmus.github.io/).

Source code can be obtained from <https://gitlab.com/dusan-gvozdenovic/librarian.git>.

Librarian is licensed under [Apache 2.0 License](../LICENSE).

@note
Documentation of this project is quite new and sparse.
See more in the [README](../README).
