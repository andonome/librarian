/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIST_BOX_H__
#define __LIST_BOX_H__

#include <ncurses.h>
#include <stdlib.h>

#include <librarian/utils.h>

/**
 * Investigate the possibility of removing sel_pos, because it can be expressed
 * as a function of top_pos and line
 */
struct list_box {
    unsigned width, height;
    unsigned pos_y, pos_x;
    unsigned top_pos;
    unsigned sel_pos;
    unsigned line;
    bool line_numbers;

    void *top;
    void *current;

    WINDOW *window;

    void *(*get_next)(void *current);
    void *(*get_prev)(void *current);
    void *(*get_first)(void);
    void *(*get_last)(void);
    unsigned (*get_size)(void);
    void (*write_line)(struct list_box *lb, void *current);
};

void list_box_init(struct list_box *lb);

void list_box_paint(struct list_box *lb);

void list_box_handle_resize(struct list_box *lb, unsigned height,
    unsigned width);

void list_box_up(struct list_box *lb);

void list_box_down(struct list_box *lb);

void list_box_top(struct list_box *lb);

void list_box_bottom(struct list_box *lb);

void list_box_goto_line(struct list_box *lb, int line);

#endif
