/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>

#include <librarian/config.h>
#include <librarian/utils.h>

#include "librarian.h"
#include "list-box.h"

void list_box_init(struct list_box *lb) {
    lb->top = lb->current = lb->get_first();
    lb->line = lb->sel_pos = lb->top_pos = 0;
}

void list_box_paint(struct list_box *lb) {
    void *curr = lb->top;
    unsigned line = 0;
    unsigned position = lb->top_pos;
    unsigned size = lb->get_size();
    unsigned pad_max = uint_digits(size);

    for (; curr && line < lb->height; curr = lb->get_next(curr), line++) {
        wmove(lb->window, lb->pos_y + line, lb->pos_x);
        if (curr == lb->current) {
            wattron(lb->window, COLOR_PAIR(SELECTION_COLOR));
        }

        if (lb->line_numbers && option_get(config.show_line_numbers)) {
            wprintw(lb->window, " %*s%d. ", pad_max - uint_digits(position + 1),
                "", position + 1);
        } else {
            wprintw(lb->window, " ");
        }
        lb->write_line(lb, curr);

        position++;

        if (curr == lb->current) {
            int x = getcurx(lb->window);
            while (x++ < lb->width) { wprintw(lb->window, " "); }
            wattroff(lb->window, COLOR_PAIR(SELECTION_COLOR));
        }
    }
}

void list_box_handle_resize(struct list_box *lb, unsigned height,
    unsigned width) {
    if (lb->width != width) { lb->width = width; }
    if (lb->height == height) { return; }
    if (!lb->top) { return; }

    if (lb->height > height) { // shrinks
        register int delta = lb->sel_pos + 2 - height;
        void *top_next = lb->get_next(lb->top);
        while (top_next && delta-- > 0) {
            lb->top = top_next;
            lb->sel_pos--;
            lb->top_pos++;
            top_next = lb->get_next(top_next);
        }
    } else { // grows
        // register int delta = height - lb->height;
        // register int delta = height - lb->sel_pos - 2;
        register int delta = lb->top_pos + height - 1 - lb->get_size();
        void *top_prev = lb->get_prev(lb->top);
        while (top_prev && delta-- > 0) {
            lb->top = top_prev;
            lb->sel_pos++;
            lb->top_pos--;
            top_prev = lb->get_prev(top_prev);
        }
    }
    lb->height = height;
    lb->width = width;
}

void list_box_up(struct list_box *lb) {
    if (!lb->current) { return; }
    void *prev = lb->get_prev(lb->current);
    if (!prev) { return; }
    void *top_prev = lb->get_prev(lb->top);
    if (top_prev && lb->sel_pos <= 2) {
        lb->top = top_prev;
        lb->top_pos--;
    } else {
        lb->sel_pos--;
    }
    lb->line--;
    lb->current = prev;
}

void list_box_down(struct list_box *lb) {
    if (!lb->current) { return; }
    void *next = lb->get_next(lb->current);
    if (!next) { return; }
    void *top_next = lb->get_next(lb->top);

    if (top_next && lb->sel_pos >= lb->height - lb->pos_y - 3 &&
        lb->top_pos + lb->height <= lb->get_size()) {
        lb->top = top_next;
        lb->top_pos++;
    } else {
        lb->sel_pos++;
    }
    lb->line++;
    lb->current = next;
}

void list_box_top(struct list_box *lb) {
    lb->top = lb->get_first();
    lb->current = lb->top;
    lb->sel_pos = 0;
    lb->top_pos = 0;
    lb->line = 0;
}

// FIX ME - convoluted logic
void list_box_bottom(struct list_box *lb) {
    lb->current = lb->get_last();

    void *top = lb->current;
    unsigned n = 0;

    while (n++ < lb->height - 2) {
        void *prev = lb->get_prev(top);
        if (!prev) { break; }
        top = prev;
    }

    lb->line = lb->get_size() - 1;
    lb->top_pos = lb->line + 1 - n;
    lb->sel_pos = min(lb->height - 2, n - 1);
    lb->top = top;
}

void list_box_goto_line(struct list_box *lb, int line) {
    int k = lb->line - line;
    if (!k) { return; }

    void *curr = lb->current;

    while (k < 0) {
        list_box_down(lb);
        if (curr == lb->current) { break; } // Detect end
        curr = lb->current;
        k++;
    }

    while (k > 0) {
        list_box_up(lb);
        if (curr == lb->current) { break; } // Detect start
        curr = lb->current;
        k--;
    }
}
