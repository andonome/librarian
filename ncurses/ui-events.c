/* SPDX-License-Identifier: Apache-2.0 */

#include <librarian/types.h>

#include "ui-events.h"

bool ui_events_equal(const struct ui_events *ue1, const struct ui_events *ue2) {
    return ue1->loop_func == ue2->loop_func &&
        ue1->paint_func == ue2->paint_func &&
        ue1->resize_func == ue2->resize_func &&
        ue1->interpret_number == ue2->interpret_number;
}
