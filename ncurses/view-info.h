/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __VIEW_INFO_H__
#define __VIEW_INFO_H__

#include "author-view.h"
#include "list-view.h"
#include "welcome.h"

#define DEFAULT_VIEW AUTHOR_VIEW_NAME

// clang-format off
static const char *views[] = {
    AUTHOR_VIEW_NAME, LIST_VIEW_NAME, WELCOME_NAME, NULL };

static const struct ui_events *ui_events[] = {
    &author_view.ui_events,
    &list_view.ui_events,
    &welcome.ui_events,
    NULL
};
// clang-format on

static inline const struct ui_events *find_wrapper(const char *name) {
    size_t i = 0;
    for (; views[i] && strcmp(views[i], name) != 0; i++) { }
    return ui_events[i];
}

#endif
