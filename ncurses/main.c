/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>

#include <librarian/core.h>
#include <librarian/localization.h>
#include <librarian/observer.h>

#include "command-mode.h"
#include "librarian.h"
#include "normal-mode.h"

static void _ui_exit(const struct lifecycle_exit_args *args);

static struct observer _on_exit;

int main(int argc, char *argv[]) {
    librarian_init();

    observer_init(&_on_exit, _ui_exit);
    observer_subscribe(&_on_exit, &lifecycle.on_exit);

    init_main_window();

    // TODO: Implement graceful exit here
    for (;;) { librarian.mode_func(); }

    librarian_exit(EXIT_SUCCESS);
}

static void _ui_exit(const struct lifecycle_exit_args *args) {
    refresh();
    curs_set(1);
    endwin();
    normal_mode_cleanup();
    command_mode_cleanup();
}
