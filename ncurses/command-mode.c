/* SPDX-License-Identifier: Apache-2.0 */

#include <assert.h>
#include <ctype.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>

#include <librarian/command.h>
#include <librarian/config.h>
#include <librarian/file.h>
#include <librarian/library.h>
#include <librarian/localization.h>
#include <librarian/log.h>
#include <librarian/option.h>

#include "command-line.h"
#include "command-mode.h"
#include "librarian.h"
#include "normal-mode.h"

static struct command_parser command_parser;

void command_mode_init(void) {
    command_parser_init(&command_parser);
    commands_core_register(&command_parser);
    command_line_init();
}

void command_mode_cleanup(void) {
    command_parser_cleanup(&command_parser);
    command_line_cleanup();
}

void command_mode_activate(void) {
    command_line_activate();
    wprintw(cmd_line.window, "%s", option_get(config.command_line_token));
    cmd_line.start_pos = u8_strlen(option_get(config.command_line_token));
    cmd_line.executor = command_mode_execute_command;
}

bool command_mode_execute_command(char *cmd_buffer) {
    skip_spaces(&cmd_buffer);
    if (*cmd_buffer == '\0' || command_mode_number(cmd_buffer)) { return true; }
    return command_parser_execute(&command_parser, cmd_buffer);
}

bool command_mode_number(char *buffer) {
    for (char *k = buffer; *k != '\0'; k++) {
        if (!isdigit(*k)) { return false; }
    }
    normal_mode_interpret_number(atoi(buffer));
    return true;
}
