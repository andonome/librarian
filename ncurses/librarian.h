/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_H__
#define __LIBRARIAN_H__

#include <librarian/config.h>
#include <librarian/log.h>
#include <librarian/types.h>

#include "command-line.h"

#define ERROR_COLOR 1
#define WARNING_COLOR 2
#define HIGHLIGHT_COLOR 3
#define INFO_COLOR 4
#define TEXT_PRIMARY_COLOR 5
#define LINE_COLOR 6
#define SELECTION_COLOR 7

enum mode { COMMAND, NORMAL, SEARCH };

struct librarian {
    enum mode mode;
    void (*mode_func)(void);
    bool ui_initialized;
};

extern struct librarian librarian;

void init_main_window(void);

void init_colors(void);

void color_changed(struct config_args *args);

enum mode get_mode(void);

void set_mode(enum mode mode);

void handle_sigint(int signum);

void handle_sighup(int signum);

void handle_sigwinch(int signum);

void handle_log_flush(const char *buffer, enum log_mode mode);

#endif
