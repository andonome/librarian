/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>
#include <signal.h>
#include <stdlib.h>

#include <librarian/compiler.h>
#include <librarian/config.h>
#include <librarian/library.h>
#include <librarian/localization.h>
#include <librarian/log.h>
#include <librarian/observer.h>
#include <librarian/types.h>

#include "author-view.h"
#include "command-line.h"
#include "command-mode.h"
#include "librarian.h"
#include "list-view.h"
#include "normal-mode.h"
#include "search-mode.h"
#include "view-info.h"
#include "welcome.h"

// clang-format off
struct librarian librarian = {
    .mode = NORMAL, .mode_func = normal_mode_loop, .ui_initialized = false
};
// clang-format on

struct observer color_observer;

#if defined(__APPLE__) && defined(__MACH__)
#define SIGWINCH 28
#endif

void init_main_window(void) {
    // Sets the terminal title
    printf("\x1b]2;%s\x07", _("Librarian"));

    if (!log_is_empty) {
        printf("%s", _("Press ENTER to continue"));
        getchar();
    }

    initscr();
    init_colors();
    struct sigaction act;

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = handle_sigint;
    sigaction(SIGINT, &act, NULL);

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = handle_sigwinch;
    sigaction(SIGWINCH, &act, NULL);

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = handle_sighup;
    sigaction(SIGHUP, &act, NULL);

    command_mode_init();
    normal_mode_init();
    log_set_flush(handle_log_flush);

    observer_init(&color_observer, color_changed);
    observer_subscribe(&color_observer, &config.on_color_change);

    // BUG: Fix me when this is properly implemented
    //  const struct ui_events *wrp = find_wrapper(*config.default_view.value);
    //  normal_mode_set_focus(library.size && wrp ? *wrp : welcome.ui_events);
    normal_mode_set_focus(
        library.size > 0 ? list_view.ui_events : welcome.ui_events);

    noecho();
    curs_set(0);

    librarian.ui_initialized = true;
}

void init_colors(void) {
    use_default_colors();
    start_color();
    init_pair(HIGHLIGHT_COLOR, option_get(config.color_highlight_fg),
        option_get(config.color_highlight_bg));

    init_pair(SELECTION_COLOR, option_get(config.color_selection_fg),
        option_get(config.color_selection_bg));

    init_pair(WARNING_COLOR, option_get(config.color_warning_fg),
        option_get(config.color_warning_bg));

    init_pair(ERROR_COLOR, option_get(config.color_error_fg),
        option_get(config.color_error_bg));

    init_pair(INFO_COLOR, option_get(config.color_info_fg),
        option_get(config.color_info_bg));

    init_pair(LINE_COLOR, option_get(config.color_highlight_bg), -1);

    init_pair(TEXT_PRIMARY_COLOR, option_get(config.color_text_primary), -1);
}

void color_changed(struct config_args *args) {
    init_colors();
    if (normal_mode.focused_object.paint_func) {
        normal_mode.focused_object.paint_func();
    }
}

enum mode get_mode(void) { return librarian.mode; }

void set_mode(enum mode mode) {
    librarian.mode = mode;
    switch (mode) {
        case COMMAND:
            command_mode_activate();
            librarian.mode_func = command_line_loop;
            break;
        case SEARCH:
            search_mode_activate();
            librarian.mode_func = command_line_loop;
            break;
        default: // Normal
            curs_set(0);
            librarian.mode_func = normal_mode_loop;
    }
}

void handle_sigint(int signum) {
    if (get_mode() == COMMAND || get_mode() == SEARCH) {
        command_line_clear();
        set_mode(NORMAL);
    }
}

void handle_sighup(int signum) { librarian_exit(EXIT_SUCCESS); }

void handle_sigwinch(int signum) {
    endwin();
    refresh();
    clear();

    normal_mode_handle_resize();
    command_line_handle_resize();
}

void handle_log_flush(const char * const buffer, enum log_mode mode) {
    // clang-format off
    int color = mode == INFO ? INFO_COLOR :
                mode == WARN ? WARNING_COLOR : ERROR_COLOR;
    // clang-format on
    command_line_message(buffer, color);
}
