/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __SEARCH_MODE_H__
#define __SEARCH_MODE_H__

#include <librarian/types.h>

void search_mode_activate(void);

bool search_mode_search(char *cmd_buffer);

void search_mode_set_search_backwards(bool backwards);

#endif
