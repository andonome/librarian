/* SPDX-License-Identifier: Apache-2.0 */

#include <inttypes.h>
#include <ncurses.h>

#include <librarian/document-types.h>
#include <librarian/document.h>
#include <librarian/library.h>
#include <librarian/localization.h>
#include <librarian/log.h>
#include <librarian/observer.h>
#include <librarian/rbtree.h>
#include <librarian/types.h>
#include <librarian/utf8.h>
#include <librarian/utils.h>
#include <librarian/vector.h>

#include "librarian.h"
#include "list-box.h"
#include "list-view.h"
#include "normal-mode.h"

#define LIST_HEIGHT (LINES - 2)
// #define TITLE_LIMIT(WIDTH) (3 * (WIDTH) / 7)
#define TITLE_LIMIT(WIDTH) (WIDTH / 2)
#define SIZE_UNUSABLE 12
#define SIZE_SMALL 47

// clang-format off
struct list_view list_view = {
    .ui_events = {
        .loop_func = list_view_loop,
        .paint_func = list_view_paint,
        .resize_func = list_view_handle_resize,
        .interpret_number = list_view_interpret_number
    },
    .document_list_box = {
        .pos_y = 1, // Start after title
        .pos_x = 0,
        .get_next = document_list_box_get_next,
        .get_prev = document_list_box_get_prev,
        .get_first = document_list_box_get_first,
        .get_last = document_list_box_get_last,
        .get_size = document_list_box_get_size,
        .write_line = document_list_box_write_line,
        .line_numbers = true
    }
};
// clang-format on

/* -------------------- implement list_box interface ------------------------ */

void *document_list_box_get_next(void *current) {
    struct library_view *view = &list_view.library_view;
    if (view->view.size == 0) { return NULL; }
    struct document **curr = current;
    struct document **last =
        document_vector_at(&view->view, view->view.size - 1);
    return curr < last ? curr + 1 : NULL;
}

void *document_list_box_get_prev(void *current) {
    struct library_view *view = &list_view.library_view;
    if (view->view.size == 0) { return NULL; }
    struct document **curr = current;
    struct document **first = document_vector_at(&view->view, 0);
    return curr > first ? curr - 1 : NULL;
}

void *document_list_box_get_first(void) {
    struct library_view *view = &list_view.library_view;
    if (view->view.size == 0) { return NULL; }
    return document_vector_at(&view->view, 0);
}

void *document_list_box_get_last(void) {
    struct library_view *view = &list_view.library_view;
    if (view->view.size == 0) { return NULL; }
    return document_vector_at(&view->view, view->view.size - 1);
}

unsigned document_list_box_get_size(void) {
    return list_view.library_view.view.size;
}

/**
 * TODO: While it works, it looks like a dumpster fire.
 *       Make it readable.
 */
void document_list_box_write_line(struct list_box *lb, void *current) {

    /* The window is too small to print anything. */
    if (lb->width <= SIZE_UNUSABLE) {
        wprintw(lb->window, "...");
        return;
    }

#define BORDER 2
#define PRINT_BORDER wprintw(lb->window, "  ")

    const struct document *doc = *(struct document **) current;

    const char *title = doc->title.c_str;
    if (!title) { path_basename(&doc->path, &title); }

    const int start = getcurx(lb->window);
    const int title_limit =
        (lb->width <= SIZE_SMALL ? lb->width - 1 : TITLE_LIMIT(lb->width)) -
        start;
    const int title_len =
        doc->title.c_str ? doc->title.length : u8_strlen(title);
    int print_width;
    int padding;

    if (title_len > title_limit) {
        print_width = u8_strnwidth(title, title_limit - 3);
        wprintw(lb->window, "%.*s...", print_width, title);
    } else {
        print_width = u8_strnwidth(title, title_len);
        padding = title_limit + print_width - title_len;
        wprintw(lb->window, "%-*.*s", padding, print_width, title);
    }

    /* The window is too small to print anything but title. */
    if (lb->width <= SIZE_SMALL) { return; }

    PRINT_BORDER;

    const int month_max_len = longest_month_name_length();
    const int date_limit = BORDER + month_max_len + BORDER + 4;
    const int author_limit =
        lb->width - start - title_limit - BORDER - date_limit - 1;

    if (doc->author.length == 0) {
        wprintw(lb->window, "%*s", author_limit, "");
    } else if (author_limit < doc->author.length) {
        print_width = u8_strnwidth(doc->author.c_str, author_limit - 3);
        wprintw(lb->window, "%.*s...", print_width, doc->author.c_str);
    } else {
        print_width = u8_strnwidth(doc->author.c_str, doc->author.length);
        padding = author_limit + print_width - doc->author.length;
        wprintw(lb->window, "%*.*s", padding, print_width, doc->author.c_str);
    }

    PRINT_BORDER;

    char * const month =
        is_no_date(doc->create_date) ? "" : month_name(doc->create_date.month);

    const int month_len = month_name_length(doc->create_date.month);
    print_width = month_name_width(doc->create_date.month);
    padding = month_max_len + print_width - month_len;
    wprintw(lb->window, "%-*.*s", padding, print_width, month);

    PRINT_BORDER;

    if (doc->create_date.year) {
        wprintw(lb->window, "%.4" PRIu32, doc->create_date.year);
    } else {
        wprintw(lb->window, "    ");
    }

#undef PRINT_BORDER
#undef BORDER
}

/* ------------------------- list_view functions ---------------------------- */

static void _list_view_load_order_from_config(void) {
    const char ** const order = option_get(config.order);
    list_view.order = (strcmp(*order, "author") == 0)
        ? document_compare_by_author
        : (strcmp(*order, "date") == 0) ? document_compare_by_date
                                        : document_compare_by_title;
}

void list_view_init(void) {
    list_view.document_list_box.width = COLS;
    list_view.document_list_box.height = LIST_HEIGHT;
    list_view.document_list_box.window = normal_mode.window;
    observer_init(&list_view.library_observer, list_view_library_updated);
    observer_init(&list_view.config_observer, list_view_config_updated);
    observer_init(&list_view.library_cleared, list_view_library_cleared);
    observer_subscribe(&list_view.library_observer, &library.subject);
    observer_subscribe(&list_view.library_cleared, &library.cleared);
    observer_subscribe(&list_view.config_observer, &config.on_change);
    list_view_init_library_view();
    list_box_init(&list_view.document_list_box);
}

// TODO: Maybe move this to library.c
void list_view_init_library_view(void) {
    size_t capacity = 10;
    size_t threshold = 7;
    if (library.size > capacity) {
        capacity = library.size * 1.5;
        threshold = library.size;
    }
    document_vector_init(&list_view.library_view.view, capacity, threshold,
        0.41);
    library_filter(&list_view.library_view, NULL);
    // Restore order
    _list_view_load_order_from_config();
    library_view_reorder(&list_view.library_view, list_view.order);
}

void list_view_library_updated(struct library_args *args) {
    struct list_box *lb = &list_view.document_list_box;
    struct library_view *view = &list_view.library_view;
    struct document **doc;
    struct document *curr_sel =
        lb->current ? *(struct document **) lb->current : NULL;

    vector_for_each_entry (doc, struct document *, args->docs) {
        document_vector_push_back_val(&view->view, *doc);
        document_ref(*doc);
    }

    // XXX: Consider replacing vectors with ordered lists
    library_view_reorder(view, list_view.order);

    struct document **new_pos = library_view_find(view, curr_sel);

    // TODO: Implement efficient seeking to current element
    list_box_init(lb);
    if (new_pos != NULL) {
        size_t line = new_pos - (struct document **) view->view.data;
        list_box_goto_line(lb, line);
    }
}

void list_view_library_cleared(void) {
    library_view_cleanup(&list_view.library_view);
    list_view_init_library_view();
    list_box_init(&list_view.document_list_box);
    if (normal_mode_in_focus(&list_view.ui_events)) { list_view_paint(); }
}

void list_view_config_updated(const struct config_args * const args) {
    if (!normal_mode_in_focus(&list_view.ui_events)) { return; }

    if (args->option == &config.order.option) {
        _list_view_load_order_from_config();
        library_view_reorder(&list_view.library_view, list_view.order);
    }

    list_view_paint();
}

void list_view_handle_resize(void) {
    list_box_handle_resize(&list_view.document_list_box, LIST_HEIGHT, COLS);
}

void list_view_activate(void) {
    if (!list_view.document_list_box.current) { return; }
    struct document *doc =
        *(struct document **) list_view.document_list_box.current;
    char cmd[2 * CONFIG_OPEN_COMMAND_FORMAT_SIZE];
    sprintf(cmd, option_get(config.open_command_format), doc->path.str.c_str);
    FILE *pipe = popen(cmd, "r");
    pclose(pipe);
}

// For reasons beyond my current understanding the terminal on my work laptop
// the first 3-12 lines would not be refreshed or become garbled if I tried to
// scroll a bit more in the list. Different terminals and values for TERM were
// tried but I did not figure it out in the end.
//
// To mitigate that and avoid hurting performance call this function only when
// making "big" changes to the underlying list_box model.
//
// OS:              macOS Ventura 13.5
// ncurses:         6.4
// TERM:            xterm-256color
// LANG:            sr_RS.UTF-8
// Terminals tried: iTerm2, kitty
static inline void _list_view_clear_pre_paint(void) {
    (void) clearok(normal_mode.window, true);
}

void list_view_paint(void) {
    werase(normal_mode.window);
    wmove(normal_mode.window, 0, 0);

    wattron(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    for (int i = 0; i < COLS; i++) { wprintw(normal_mode.window, " "); }

    mvwprintw(normal_mode.window, 0, 1, "%s", _("List View"));

    wattroff(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    list_box_paint(&list_view.document_list_box);

    wattron(normal_mode.window, COLOR_PAIR(LINE_COLOR));

    wmove(normal_mode.window, LINES - 2, 0);
    whline(normal_mode.window, ACS_HLINE, COLS);

    wattroff(normal_mode.window, COLOR_PAIR(LINE_COLOR));
    doupdate();
}

void list_view_loop(utf8_char ch) {
    switch (ch) {
        case KEY_UP:
        case 'k':
            list_box_up(&list_view.document_list_box);
            list_view_paint();
            break;
        case KEY_DOWN:
        case 'j':
            list_box_down(&list_view.document_list_box);
            list_view_paint();
            break;
        case 'G':
            list_box_bottom(&list_view.document_list_box);
            _list_view_clear_pre_paint();
            list_view_paint();
            break;
        case 'g':
            if (wgetch(normal_mode.window) == 'g') {
                list_box_top(&list_view.document_list_box);
                _list_view_clear_pre_paint();
                list_view_paint();
            }
            break;
        case 10:
            list_view_activate();
            break;
    }
}

void list_view_interpret_number(int number) {
    list_box_goto_line(&list_view.document_list_box, number - 1);
    _list_view_clear_pre_paint();
    list_view_paint();
}

void list_view_cleanup(void) { library_view_cleanup(&list_view.library_view); }
