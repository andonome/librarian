/* SPDX-License-Identifier: Apache-2.0 */

#include "search-mode.h"
#include "command-line.h"
#include "list-box.h"
#include "list-view.h"

#include <string.h>

#include <librarian/document-types.h>
#include <librarian/document.h>
#include <librarian/library.h>
#include <librarian/limits.h>
#include <librarian/types.h>
#include <librarian/u-string.h>
#include <librarian/utils.h>

static bool search_backwards = false;

void search_mode_activate(void) {
    command_line_activate();
    wprintw(cmd_line.window, "%s", search_backwards ? "?" : "/");
    cmd_line.start_pos = 1;
    cmd_line.executor = search_mode_search;
}

static char search_query[PATH_MAX];

bool search_predicate(struct document *doc) {
    return (doc->title.c_str && !!strstr(doc->title.c_str, search_query)) ||
        (doc->author.c_str && !!strstr(doc->author.c_str, search_query)) ||
        (doc->path.str.c_str && !!strstr(doc->path.str.c_str, search_query));
}

// XXX: We should not touch the list_view from here but it should work
// temporarily.
bool search_mode_search(char *cmd_buffer) {
    if (string_is_null_or_empty(cmd_buffer)) {
        library_view_cleanup(&list_view.library_view);
        list_view_init_library_view();
    } else {
        strncpy(search_query, cmd_buffer, PATH_MAX - 1);
        library_view_cleanup(&list_view.library_view);
        size_t capacity = 10;
        size_t threshold = 7;
        if (library.size > capacity) {
            capacity = library.size * 1.5;
            threshold = library.size;
        }
        document_vector_init(&list_view.library_view.view, capacity, threshold,
            0.41);
        library_filter(&list_view.library_view, search_predicate);
    }
    list_box_top(&list_view.document_list_box);
    list_view_paint();

    return true;
}

void search_mode_set_search_backwards(bool backwards) {
    search_backwards = backwards;
}
