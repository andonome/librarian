/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __AUTHOR_VIEW_H__
#define __AUTHOR_VIEW_H__

#include <librarian/utf8.h>
#include <ncurses.h>

#include "ui-events.h"

#define AUTHOR_VIEW_NAME "author-view"

struct author_view {
    unsigned position;
    struct ui_events ui_events;
};

extern struct author_view author_view;

void author_view_init(void);

void author_view_paint(void);

void author_view_loop(utf8_char ch);

#endif
