/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIST_VIEW_H__
#define __LIST_VIEW_H__

#include <librarian/config.h>
#include <librarian/library.h>
#include <librarian/utf8.h>

#include "list-box.h"
#include "ui-events.h"

#define LIST_VIEW_NAME "list-view"

struct list_view {
    struct ui_events ui_events;
    struct library_view library_view;
    struct observer library_observer;
    struct observer library_cleared;
    struct observer config_observer;
    struct list_box document_list_box;
    lib_compare order;
};

extern struct list_view list_view;

void *document_list_box_get_next(void *current);

void *document_list_box_get_prev(void *current);

void *document_list_box_get_first(void);

void *document_list_box_get_last(void);

unsigned document_list_box_get_size(void);

void document_list_box_write_line(struct list_box *lb, void *current);

void list_view_init(void);

void list_view_init_library_view(void);

void list_view_library_updated(struct library_args *args);

void list_view_library_cleared(void);

void list_view_config_updated(const struct config_args *args);

void list_view_handle_resize(void);

void list_view_activate(void);

void list_view_paint(void);

void list_view_loop(utf8_char ch);

void list_view_interpret_number(int number);

void list_view_cleanup(void);

#endif
