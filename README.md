# Librarian
## A dead-simple document organizer.
Librarian is an ncurses-based program that helps you organize, tag, view and search your documents. Heavily inspired by [vim](https://www.vim.org) and [cmus](https://cmus.github.io/).

![Screenshot](docs/screenshot.png)
## Building & installation
Packages for the popular distros are planned and on their way, but for now librarian must be built from source.

### Dependencies

#### Building
|     Package     |    Version     |                   License                    |
|-----------------|----------------|----------------------------------------------|
| cmake           | 3.13 or later  | [BSD-3-Clause][cmake-license]                |
| gettext         | 0.19 or later  | [GPL-3.0+][gettext-license]                  |

You will also need a build system of your choice supported by CMake (e.g. GNU Make, Ninja, ...)

#### Program
|     Package     |    Version     |                   License                    |
|-----------------|----------------|----------------------------------------------|
| libintl         | 0.19 or later  | [LGPL-2.1-only][libintl-license]             |
| ncurses         | 6.0 or later   | [Custom Permissive License][ncurses-license] |
| exempi          | 2.4.3 or later | [BSD-3-Clause][exempi-license]               |

[cmake-license]: https://gitlab.kitware.com/cmake/cmake/raw/master/Copyright.txt
[exempi-license]: https://raw.githubusercontent.com/hfiguiere/exempi/master/BSD-License.txt
[gettext-license]: https://raw.githubusercontent.com/autotools-mirror/gettext/master/COPYING
[ncurses-license]: https://raw.githubusercontent.com/mirror/ncurses/master/COPYING
[libintl-license]: https://raw.githubusercontent.com/autotools-mirror/gettext/master/gettext-runtime/intl/COPYING.LIB

### Procedure
```bash
git clone https://gitlab.com/dusan-gvozdenovic/librarian.git
cd librarian

mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
```

### Optional features

You may also pass the following options to CMake:
 
| Option                        | Purpose                                  | Default | Dependencies             |
| ------------------------------|------------------------------------------|---------|--------------------------|
| `ENABLE_DOCS`                 | Enable building of documentation.        | `OFF`   | Doxygen                  |
| `ENABLE_EXPERIMENTAL_PLUGINS` | Enable building of experimental plugins. | `OFF`   | N/A                      |
| `ENABLE_LOCALIZATION`         | Enable localization support.             | `ON`    | gettext                  |
| `ENABLE_TESTS`                | Enable building of tests.                | `OFF`   | CUnit, tmux, imagemagick |
