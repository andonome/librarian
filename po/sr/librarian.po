# Serbian translations for PACKAGE package.
# Copyright (C) 2018 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#  <dusan.gvozdenovic.99@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-12 23:06+0200\n"
"PO-Revision-Date: 2018-07-11 15:22+0200\n"
"Last-Translator:  <dusan.gvozdenovic.99@gmail.com>\n"
"Language-Team: Serbian\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:279
#, c-format
msgid "\"%s\" requires API version "
msgstr "\"%s\" захтева верзију АПИ-а"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:33
msgid "April"
msgstr "Април"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:37
msgid "August"
msgstr "Август"

#: /Volumes/Data/Projects/librarian/ncurses/author-view.c:35
msgid "Authors"
msgstr "Аутори"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:331
#, c-format
msgid "Circular reference detected between plugins \"%s\" and \"%s\"."
msgstr "Уочена је кружна зависност између додатака \"%s\" и \"%s\"."

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:270
#, c-format
msgid "Could not find symbol __librarian_plugin_decl. Reason: %s."
msgstr "Нисам успео да пронађем симбол __librarian_plugin_decl. Разлог: %s"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:355
#, c-format
msgid "Could not find the dependency \"%s\"."
msgstr "Нисам успео да пронађем зависност \"%s\"."

#: /Volumes/Data/Projects/librarian/librarian/localization.c:41
msgid "December"
msgstr "Децембар"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:380
#, c-format
msgid "Dependency \"%s\" (%s) is too old. Expected version %s."
msgstr "Зависност \"%s\" (%s) је превише стара. Очекивао сам верзију %s."

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:394
#, c-format
msgid "Dependency \"%s\" (%s) is too recent. Expected version %s."
msgstr "Зависност \"%s\" (%s) је превише нова. Очекивао сам верзију %s."

#: /Volumes/Data/Projects/librarian/librarian/log.c:32
msgid "Error"
msgstr "Грешка"

#: /Volumes/Data/Projects/librarian/librarian/command.c:359
msgid "Expected a mnemonic and a command string!"
msgstr "Очекивао сам мнемоник i командну ниску!"

#: /Volumes/Data/Projects/librarian/librarian/command.c:378
msgid "Expected a plugin identifier or path!"
msgstr "Очекивао сам идентификатор додатка или путању!"

#: /Volumes/Data/Projects/librarian/librarian/command.c:229
#: /Volumes/Data/Projects/librarian/librarian/command.c:292
#: /Volumes/Data/Projects/librarian/librarian/command.c:403
msgid "Expected an identifier!"
msgstr "Очекивао сам идентификатор!"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:75
#, c-format
msgid "Failed to close library handle of plugin \"%s\". Reason: %s"
msgstr "Нисам успео да затворим ручку библиотеке додатка \"%s\". Разлог: %s"

#: /Volumes/Data/Projects/librarian/librarian/config.c:151
msgid "Failed to create config directory. Exiting now."
msgstr "Нисам успео да креирам конфигурациони директоријум. Излазим сада."

#: /Volumes/Data/Projects/librarian/librarian/config.c:160
msgid "Failed to create config file. Exiting now."
msgstr "Нисам успео да креирам конфигурациону датотеку. Излазим сада."

#: /Volumes/Data/Projects/librarian/librarian/xmalloc.c:36
#, c-format
msgid "Failed to duplicate string \"%s\". Exiting now."
msgstr "Нисам успео да дуплицирам ниску \"%s\". Излазим сада."

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:502
#, c-format
msgid "Failed to find plugin \"%s\"."
msgstr "Нисам успео да пронађем додатак \"%s\"."

#: /Volumes/Data/Projects/librarian/librarian/config.c:181
#, c-format
msgid "Failed to open config file \"%s\" for reading."
msgstr "Неуспело отварање конфигурационе датотеке за читање."

#: /Volumes/Data/Projects/librarian/librarian/library.c:78
msgid "Failed to open library file for reading."
msgstr "Неуспело отварање датотеке библиотеке за читање."

#: /Volumes/Data/Projects/librarian/librarian/library.c:202
msgid "Failed to open library file for writing."
msgstr "Неуспело отварање датотеке библиотеке за писање."

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:263
#, c-format
msgid "Failed to open library: %s"
msgstr "Неуспело отварање датотеке библиотеке за читање."

#: /Volumes/Data/Projects/librarian/librarian/config.c:198
#, c-format
msgid "Failed to parse \"%s\"."
msgstr "Неуспело рашчлањивање \"%s\"."

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:253
#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:508
#, c-format
msgid "Failed to parse plugin identifier. Invalid library name: \"%s\"."
msgstr ""
"Нисам успео да рашчланим идентификатор додатка. Неисправно име библиотеке "
"\"%s\"."

#: /Volumes/Data/Projects/librarian/librarian/localization.c:31
msgid "February"
msgstr "Фебруар"

#: /Volumes/Data/Projects/librarian/librarian/command.c:200
msgid "Invalid path specified."
msgstr "Унета је неисправна путања."

#: /Volumes/Data/Projects/librarian/librarian/localization.c:30
msgid "January"
msgstr "Јануар"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:36
msgid "July"
msgstr "Јул"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:35
msgid "June"
msgstr "Јун"

#. Sets the terminal title
#: /Volumes/Data/Projects/librarian/ncurses/librarian.c:39
#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:110
msgid "Librarian"
msgstr "Библиотекар"

#: /Volumes/Data/Projects/librarian/ncurses/list-view.c:289
msgid "List View"
msgstr "Приказ листе"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:32
msgid "March"
msgstr "Март"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:34
msgid "May"
msgstr "Мај"

#: /Volumes/Data/Projects/librarian/librarian/command.c:188
msgid "No path specified."
msgstr "Није наведена путања."

#: /Volumes/Data/Projects/librarian/librarian/localization.c:40
msgid "November"
msgstr "Новембар"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:39
msgid "October"
msgstr "Октобар"

#. FIXME: How safe is this?
#: /Volumes/Data/Projects/librarian/librarian/command.c:321
#, c-format
msgid "Option \"%s\" does not exist!"
msgstr "Опција \"%s\" не постоји!"

#. FIXME: How safe is this?
#: /Volumes/Data/Projects/librarian/librarian/command.c:327
#, c-format
msgid "Option \"%s\" is not a boolean type!"
msgstr "Опција \"%s\" није Буловог типа!"

#: /Volumes/Data/Projects/librarian/librarian/command.c:415
#, c-format
msgid "Plugin \"%s\" cannot be unloaded. Some other plugin is using it."
msgstr "Додатак \"%s\" не може бити искључен. Неки други додатак га користи."

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:519
#, c-format
msgid "Plugin \"%s\" is already loaded."
msgstr "Додатак \"%s\" је већ учитан."

#: /Volumes/Data/Projects/librarian/librarian/command.c:413
#, c-format
msgid "Plugin \"%s\" is not loaded."
msgstr "Додатак \"%s\" није учитан!"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:461
#, c-format
msgid "Plugin's load() function returned non-zero exit code (%d): %s."
msgstr "load() функција додатка је вратила не-нула излазни код (%d): %s."

#: /Volumes/Data/Projects/librarian/ncurses/librarian.c:42
msgid "Press ENTER to continue"
msgstr "Притисните ЕНТЕР да бисте наставили"

#: /Volumes/Data/Projects/librarian/librarian/localization.c:38
msgid "September"
msgstr "Септембар"

#. XXX: We can issue an appropriate migration here later, but for now we
#. * only show warning.
#: /Volumes/Data/Projects/librarian/librarian/library.c:109
#, c-format
msgid ""
"The library version %llu is greater than librarian version %llu. Consider "
"updating librarian."
msgstr ""
"Верзија библиотеке %llu је већа од верзије библиотекара %llu. Размотри "
"ажурирање библиотекара."

#: /Volumes/Data/Projects/librarian/ncurses/author-view.c:38
msgid "Titles"
msgstr "Наслови"

#: /Volumes/Data/Projects/librarian/librarian/command.c:99
msgid "Unexpected end of line. Expected to read \"."
msgstr "Неочекиван крај линије. Очекивао сам да прочитам \"."

#: /Volumes/Data/Projects/librarian/librarian/command.c:173
#, c-format
msgid "Unexpected string \"%.*s\" after statement!"
msgstr "Неочекивана ниска \"%.*s\" после исказа!"

#: /Volumes/Data/Projects/librarian/ncurses/command-line.c:252
msgid "Unknown command specified!"
msgstr "Унета је непостојећа команда!"

#: /Volumes/Data/Projects/librarian/librarian/log.c:30
msgid "Warning"
msgstr "Упозорење"

#: /Volumes/Data/Projects/librarian/librarian/command.c:342
msgid "boolean value, 'true' or 'false' expected!"
msgstr "Булова вредност 'true' или 'false' је очекивана!"

#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:115
msgid "by Dusan Gvozdenovic"
msgstr "написао Душан Гвозденовић"

#: /Volumes/Data/Projects/librarian/librarian/command.c:337
msgid "color or integer in range -1 ... 255 expected!"
msgstr "боја или цео број у опсегу -1 ... 255 је очекиван!"

#: /Volumes/Data/Projects/librarian/librarian/command.c:248
msgid "expected a value!"
msgstr "очекивана је вредност!"

#: /Volumes/Data/Projects/librarian/librarian/command.c:332
msgid "expected an integer value!"
msgstr "очекивана је целобројна вредност!"

#: /Volumes/Data/Projects/librarian/ncurses/normal-mode.c:99
msgid "files added"
msgstr "датотека додато"

#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:82
msgid "for help"
msgstr "за помоћ"

#: /Volumes/Data/Projects/librarian/librarian/command.c:347
msgid "invalid enum value!"
msgstr "неисправна набројива вредност!"

#: /Volumes/Data/Projects/librarian/librarian/xmalloc.c:27
msgid "librarian failed to allocate requested memory. Exiting now.\n"
msgstr ""
"библиотекар није успео да алоцира тражену количину меморије. Излазим сада.\n"

#: /Volumes/Data/Projects/librarian/librarian/plugin-registry.c:287
msgid "plugin does not implement load() function."
msgstr "додатак не имплементира load() функцију."

#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:84
msgid "to add documents to the library"
msgstr "за додавање докумената у библиотеку"

#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:80
msgid "to quit"
msgstr "за излаз"

#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:47
msgid "type"
msgstr "откуцајте"

#: /Volumes/Data/Projects/librarian/ncurses/welcome.c:113
msgid "version"
msgstr "верзија"
