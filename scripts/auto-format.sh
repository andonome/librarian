#!/bin/bash

set -e

CLANG_FORMAT="clang-format"

if ! [ -x $(command -v "${CLANG_FORMAT}") ]; then
    echo "clang-format not found!" >&2
    exit 2
fi

PROJECT_ROOT=$(git rev-parse --show-toplevel)
SOURCE_DIRS=("librarian" "ncurses")

format() {
    [ -z "$( ${CLANG_FORMAT} -n $1 2>&1)" ] && return
    printf "Formatting %s\n" "$(realpath --relative-to=${PROJECT_ROOT} $1)"
    "${CLANG_FORMAT}" -i "$1"
    # Move this somewhere else
    git add "$1"
}

for dir in "${SOURCE_DIRS[@]}"; do
    FILES=$(find ${PROJECT_ROOT}/${dir} \( -name '*.c' -o -name '*.h' \))
    for s in ${FILES}; do format "${s}"; done
done
