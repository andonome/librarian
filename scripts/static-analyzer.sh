#!/bin/bash

set -x

SCAN_BUILD="/usr/bin/env scan-build -enable-checker alpha,core,deadcode,security,unix"

if ! ${SCAN_BUILD} $@; then
    echo "Failed to run scan-build" >2
    exit 1
fi
